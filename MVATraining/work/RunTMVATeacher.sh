#!/bin/bash

#### MAKE SURE YOU AREN'T OVERWRITING A CLASSIFIER THAT YOU HAVEN'T SAVED SOMEWHERE, IF YOU NEED IT #### 

# Choose mode here (type in lower case)
# Will need to add years into this, as I need 6 different classifiers (1 per year)

year=2018
mode=dd

# The rest does itself just fine, no need (in theory) to edit anything)
modecaps=${mode^^}
mkdir -p output-$modecaps
export WORKDIR="DataSet/output-$modecaps/weights/"
mkdir -p $WORKDIR
cp TMVAInputs$modecaps.txt $WORKDIR
# For some reason plots go into DataSet/plots, so they get overwritten, however as the output root file goes into output-XX they can be regenerated easily

# Run the teacher - now running on a precombined and preselected (nb the background training data is KPiPi & 3Pi data, high mass sideband cut applied here)
TMVATeacher.exe --TrainingParameters TMVATraining.txt --Inputs TMVAInputs$modecaps.txt --SignalTrainingFile training-files/signalfilesnew$mode.txt --BckgTrainingFile training-files/backgroundfilesnew$mode.txt --SignalCuts cuts/signalcutsnew.txt --BckgCuts cuts/backgroundcutsnew.txt --OutputDir output-$modecaps 2>&1 | tee logs/TMVATrain$modecaps.log 

exit 0

# checklist of updates for new pipeline and new mc/data (oct 2019)
# TMVATraining.txt - just the 3 (BDTA/BDTG/BDTA) for use with the optimiser - fine
# TMVAInputsDD/LL.txt - as before - for a first pass this is fine (will complicate, add logs, etc later)
# TMVAInputs3piDD/LL.txt - as before - for a first pass this is fine (will complicate, add logs, etc later)
# training-files/signalfilesnewLL/DD.txt - changed to use rootfiles/combined/mva-2018-sig-ll/dd.root
# training-files/backgroundfilesnewLL/DD.txt - changed to use rootfiles/combined/mva-2018-bg-ll/dd.root (if not combining, get 1600 bg vs 8000 sig)
# cuts/signalcutsnew.txt - as before (blank)
# cuts/backgroundcutsnew.txt - B_PVFit_M -> B_PVFit_Mass
