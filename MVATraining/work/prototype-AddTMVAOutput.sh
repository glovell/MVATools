#!/bin/bash

# This applies the output of the current TMVA classifer to all 32 data files from run 1. It does not 


# Put in some code to potentially make the directories in question "mkdir -p <name>"



# Comment out one of the arrays
# This is a list of all the file names that exist all the filtered-real-vX and filtered-mc-vX (and just filtered-real/filtered-mc), but not the path. Run I only
#declare -a arr=(
#"Real2012MagUp_BDhhh_2012_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2012MagUp_BDhhh_2012_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2012MagDown_BDhhh_2012_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2012MagDown_BDhhh_2012_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2011MagUp_BDhhh_2011_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2011MagUp_BDhhh_2011_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2011MagDown_BDhhh_2011_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2011MagDown_BDhhh_2011_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2012MagUp_BDhhh_2012_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2012MagUp_BDhhh_2012_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2012MagDown_BDhhh_2012_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2012MagDown_BDhhh_2012_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2011MagUp_BDhhh_2011_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2011MagUp_BDhhh_2011_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2011MagDown_BDhhh_2011_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2011MagDown_BDhhh_2011_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP8_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP8_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP8_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP8_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP6_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP6_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP6_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP6_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP8_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP8_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP8_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP8_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP6_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP6_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP6_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP6_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#)

# This is a list of the file names for just real signal data (for quicker tests). Run I only
#declare -a arr=(
#"Real2012MagUp_BDhhh_2012_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2012MagUp_BDhhh_2012_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2012MagDown_BDhhh_2012_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2012MagDown_BDhhh_2012_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2011MagUp_BDhhh_2011_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2011MagUp_BDhhh_2011_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2011MagDown_BDhhh_2011_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2011MagDown_BDhhh_2011_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP8_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP8_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP8_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP8_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP6_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP6_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP6_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP6_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#)

# This is a list of the 3pi data (for updates to the above subset). Run I only
#declare -a arr=(
#"Real2012MagUp_BDhhh_2012_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2012MagUp_BDhhh_2012_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2012MagDown_BDhhh_2012_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2012MagDown_BDhhh_2012_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2011MagUp_BDhhh_2011_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2011MagUp_BDhhh_2011_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"Real2011MagDown_BDhhh_2011_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"Real2011MagDown_BDhhh_2011_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP8_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP8_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP8_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP8_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP6_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MuP6_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP6_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"MC_BDhhh_MdP6_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#)
# I edited the cuts and re-added the Ds veto so I'm sure the PiPiPi stuff won't work any more

# Run II data files (no MC, no 2018, no WS for 2015) NB change real-data-v2 to real-data-v3 in infilename
#declare -a arr=(
#"RealMagUp_BDhhh_2015_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2015_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2015_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2015_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2015_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2015_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2015_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2015_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDKPiPi_DPiPiKSLL_WS.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDKPiPi_DPiPiKSDD_WS.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDPiPiPi_DPiPiKSLL_WS.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDPiPiPi_DPiPiKSDD_WS.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDKPiPi_DPiPiKSLL_WS.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDKPiPi_DPiPiKSDD_WS.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDPiPiPi_DPiPiKSLL_WS.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDPiPiPi_DPiPiKSDD_WS.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDKPiPi_DPiPiKSLL_WS.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDKPiPi_DPiPiKSDD_WS.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDPiPiPi_DPiPiKSLL_WS.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDPiPiPi_DPiPiKSDD_WS.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDKPiPi_DPiPiKSLL_WS.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDKPiPi_DPiPiKSDD_WS.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDPiPiPi_DPiPiKSLL_WS.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDPiPiPi_DPiPiKSDD_WS.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#)

# Run II data files (no MC, no 2018, no WS for 2015) NB change real-data-v2 to real-data-v3 in infilename
#declare -a arr=(
#"RealMagUp_BDhhh_2015_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2015_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2015_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2015_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2015_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2015_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2015_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2015_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2016_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2016_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagUp_BDhhh_2017_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
#"RealMagDown_BDhhh_2017_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
#)
declare -a arr=(
"MC_BDhhh_2017_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
"MC_BDhhh_2017_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
"MC_BDhhh_2017_BDPiPiPi_DPiPiKSDD.tuples.root:BDPiPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
"MC_BDhhh_2017_BDPiPiPi_DPiPiKSLL.tuples.root:BDPiPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"
)
# This loops through each name, figures out whether each is LL/DD, sig/bg, real/mc, and then sets the appropriate
for name in "${arr[@]}"
do
	echo "================================================================================================================"
	echo "Running $name"
	# This lot just works out some details so everything can be run in one go
	# LL or DD - this doesn't require its own nested if when running, as the correct bit is directly appended to the strings run
	if echo "$name" | grep -q "LL"; then
		mode="ll";
	else
		mode="dd";
	fi
	echo "The datatype is $mode"
	# This selects real/simulated data
	if echo "$name" | grep -q "Real"; then
		echo "This is real data"
		infilename="/r01/lhcb/lovell/b2dkpipi/MC/prototyping/run-20/dsts-run-20/rootfiles/filter-2-applied/real-v9/$name"
		real=true;
	else
		echo "This is simulated data"
		infilename="/r01/lhcb/lovell/b2dkpipi/MC/prototyping/run-20/dsts-run-20/rootfiles/filter-2-applied/mc-v9/$name"
		real=false;
	fi
	# This selects KPiPi (signal)/PiPiPi (not signal) data
	if echo "$name" | grep -q "KPiPi_"; then
		echo "This is KPiPi data"
		signal=true;
	else
		echo "This is PiPiPi data"
		signal=false;
	fi
	# Setting some names that are used later
	tree=${name#*:}
	namenotree=${name%%:*}
	modecaps=${mode^^}
	outfilename="/r01/lhcb/lovell/b2dkpipi/MC/prototyping/run-20/dsts-run-20/rootfiles/bdt-applied/$namenotree" ######### NB NB NB make sure you create this directory first, or you get 32 segmentation violations which isn't pretty###############
	echo "================================================================================================================"

	# Deciding between KPiPi and PiPiPi, and running the correct one
	if [ "$signal" = true ]; then
		# Running KPiPi
		cp TMVAInputs$modecaps.txt DataSet/output-$modecaps/weights/
		# the cuts used to be signalcuts$mode.txt, but now that cuts are pre-applied it doesn't matter any more
		TMVAReader.exe --TreeName $tree --Inputs TMVAInputs$modecaps.txt --InRootFiles $infilename --OutRootFile $outfilename --WorkDir DataSet/output-$modecaps/weights --PreSelCutsFile cuts/signalcutsnew.txt 2>&1 | tee logs/TMVARead$namenotree.log
	else
		# Running PiPiPi - NB if I ever change the M(hhh)<2GeV cut, the 3pi data will need to be changed, as that cut is pre-applied
		cp TMVAInputs3Pi$modecaps.txt DataSet/output-$modecaps/weights/
		# The cuts used to be signalcuts3pi$mode.txt, however now cuts are pre-applied this isn't releveant any more
		TMVAReader.exe --TreeName $tree --Inputs TMVAInputs3Pi$modecaps.txt --InRootFiles $infilename --OutRootFile $outfilename --WorkDir DataSet/output-$modecaps/weights --PreSelCutsFile cuts/signalcutsnew.txt 2>&1 | tee logs/TMVARead$namenotree.log
	fi
done #Ends the loop over the array of file names

# This could do the hadd step for you, but as it takes no time, and is just another place I'd need to edit the script each time, I won't bother
# The commands would be:
#hadd -ff /r01/lhcb/lovell/b2dkpipi/rootfiles/bdt-applied/v2/all-real-signal-LL.root /r01/lhcb/lovell/b2dkpipi/bdt-applied/v2/Real*KPiPi*LL*
#hadd -ff /r01/lhcb/lovell/b2dkpipi/rootfiles/bdt-applied/v2/all-real-signal-DD.root /r01/lhcb/lovell/b2dkpipi/bdt-applied/v2/Real*KPiPi*DD*

echo "All done!"

exit 0



## Everything below this worked perfectly before I put in the loop, just remove 1 # from the start of each line (now would also require some significant rearranging above + moving above the exit command)
## Again, choose mode here (lower case)
#mode=dd
## Copy in the full file name from the list above, into quotes:
#name="Real2011MagDown_BDhhh_2011_BDKPiPi_DPiPiKSDD.tuples.root:BDKPiPi_DPiPiKSDD_Tuple/BDKPiPiTuple"
## If this is PiPiPi data, then change which TMVAReader.exe line is commented, and 
## If this is MC data, the infilename will have to be changed from filtered-real to filtered-mc
#
#tree=${name#*:}
#namenotree=${name%%:*}
#infilename="/r01/lhcb/lovell/b2dkpipi/rootfiles/filtered-real-v3/$name"
#outfilename="/r01/lhcb/lovell/b2dkpipi/rootfiles/bdt-applied-7/$namenotree"
#modecaps=${mode^^}
#
## KPiPi
#cp TMVAInputs$modecaps.txt DataSet/output-$modecaps/weights/
#TMVAReader.exe --TreeName $tree --Inputs TMVAInputs$modecaps.txt --InRootFiles $infilename --OutRootFile $outfilename --WorkDir DataSet/output-$modecaps/weights --PreSelCutsFile signalcuts$mode.txt 2>&1 | tee logs/TMVARead$namenotree.log
#
## PiPiPi
##cp TMVAInputs3Pi$modecaps.txt DataSet/output-$modecaps/weights/
##TMVAReader.exe --TreeName $tree --Inputs TMVAInputs3Pi$modecaps.txt --InRootFiles $infilename --OutRootFile $outfilename --WorkDir DataSet/output-$modecaps/weights --PreSelCutsFile signalcuts3pi$mode.txt 2>&1 | tee logs/TMVARead$namenotree.log
