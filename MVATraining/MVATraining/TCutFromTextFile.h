
#pragma once

// STL
#include <string>
#include <iostream>

// ROOT
#include "TCut.h"

/// Create a TCut object from a list of cuts in a text file
bool createTCut( const std::string & cuts, TCut & sel );
