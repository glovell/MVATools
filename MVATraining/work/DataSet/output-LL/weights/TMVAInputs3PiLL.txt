# List of input variables to feed to TMVA for the LL analysis (2 more than for the DD analysis), edited so that it /should/ be able to work to apply the MVAs to 3Pi data
# Lines starting with a "#" and blank lines are ignored. Format is 		MyVarName	;	F/I	;	ExpressionForVar, e.g. "MinMuIPChi2 ; F ; sqrt(TMath::Min(muplus_MINIPCHI2,muminus_MINIPCHI2))"

# List: 	B: ipchi2 pv, fdchi2, b pv fit chi2, pt asymmetry cone 1.5
#			KPiPi: ipchi2 pv, vertex chi2/ndf
#			D0: (Z_D-Z_B)/error -sq, add, sqrt
#			K, Pi, Pi: min(ipchi2 pv), min(p_transverse)
#			Ddaughter pions: min(ipchi2 pv), max(ipchi2 pv)
#			max(cos phi) between bach daughter track and D0. Susan: take dot product of the mmm vectors, divided by the product of the momentum magnitudes
#			Mass(h1- h2+), Mass(h2+ h3-) of bach hadrons
#			KS daughter pions: min(ipchi2 pv), max(ipchi2 pv) - LL only

B_ipchi2_ownpv				; F ; B_IPCHI2_OWNPV
B_fdchi2_ownpv				; F ; B_FDCHI2_OWNPV
B_PVFit_chi2_div_ndof		; F ; (B_PVFit_chi2/B_PVFit_nDOF)
B_PT_ASYMMETRY				; F ; B_CONE1PTASY
K_1_1270_ipchi2_ownpv		; F ; a_1_1260_plus_IPCHI2_OWNPV
K_1_1270_plus_VERTEX		; F ; (a_1_1260_plus_ENDVERTEX_CHI2/a_1_1260_plus_ENDVERTEX_NDOF)
Z_D_minus_Z_B_div_error		; F ; (D0_ENDVERTEX_Z-B_ENDVERTEX_Z)/(TMath::Sqrt(D0_ENDVERTEX_ZERR*D0_ENDVERTEX_ZERR+B_ENDVERTEX_ZERR*B_ENDVERTEX_ZERR))
Bach_Min_IPCHI2_PV			; F ; TMath::Min(hBach_IPCHI2_OWNPV,TMath::Min(PiPlusBach_IPCHI2_OWNPV,PiMinusBach_IPCHI2_OWNPV))
Bach_Min_PT					; F ; TMath::Min(hBach_PT,TMath::Min(PiPlusBach_PT,PiMinusBach_PT))
DdauPi_Min_IPCHI2_PV		; F ; TMath::Min(DdauPlus_IPCHI2_OWNPV,DdauMinus_IPCHI2_OWNPV)
DdauPi_Max_IPCHI2_PV		; F ; TMath::Max(DdauPlus_IPCHI2_OWNPV,DdauMinus_IPCHI2_OWNPV)
Max_cos_phi_D0_bach			; F ; TMath::Max((D0_PX*hBach_PX+D0_PY*hBach_PY+D0_PZ*hBach_PZ)/(D0_P*hBach_P),TMath::Max((D0_PX*PiPlusBach_PX+D0_PY*PiPlusBach_PY+D0_PZ*PiPlusBach_PZ)/(D0_P*PiPlusBach_P),(D0_PX*PiMinusBach_PX+D0_PY*PiMinusBach_PY+D0_PZ*PiMinusBach_PZ)/(D0_P*PiMinusBach_P))) 
Massh1h2					; F ; TMath::Sqrt(hBach_M^2 + PiMinusBach_M^2 + 2*(hBach_PE*PiMinusBach_PE) - 2* (hBach_PX*PiMinusBach_PX + hBach_PY*PiMinusBach_PY + hBach_PZ*PiMinusBach_PZ)) 
Massh2h3					; F ; TMath::Sqrt(PiPlusBach_M*PiPlusBach_M + PiMinusBach_M*PiMinusBach_M + 2*(PiPlusBach_PE*PiMinusBach_PE) - 2*(PiPlusBach_PX*PiMinusBach_PX + PiPlusBach_PY*PiMinusBach_PY + PiPlusBach_PZ*PiMinusBach_PZ))
KSdau_Min_IPCHI2_PV			; F ; TMath::Min(KSdauPlus_IPCHI2_OWNPV,KSdauMinus_IPCHI2_OWNPV)
KSdau_Max_IPCHI2_PV			; F ; TMath::Max(KSdauPlus_IPCHI2_OWNPV,KSdauMinus_IPCHI2_OWNPV)
