
// STL
#include <fstream>
#include <iostream>
#include <string>

// local
#include "MVATraining/TCutFromTextFile.h"
#include "MVATraining/GetTime.h"

// boost
#include <boost/algorithm/string.hpp>

bool createTCut( const std::string & cuts, TCut & sel )
{
  bool ok = true;

  // Full selection string
  std::string fullSelection;

  // Do we have a text file listing cuts, or text file(s) containing the cuts
  if ( cuts.find(".txt") != std::string::npos )
  {

    // Decompose a comma seperated list into a vector of strings
    std::vector<std::string> files;
    boost::split( files, cuts, boost::is_any_of(",") );

    // Loop over cuts files
    for ( auto& file : files )
    {
      if ( !file.empty() )
      {

        // Open the cuts text file
        std::ifstream cutStream( file.c_str() );

        // If OK process
        if ( !cutStream.is_open() )
        {
          ok = false;
          std::cerr << getTime() << "Failed to open " << file << std::endl;
        }
        else
        {

          std::cout << getTime() 
                    << "Creating selection from '" << file << "'" << std::endl;

          std::string cut;
          while ( std::getline(cutStream,cut) )
          {
            if ( cut.empty() ) continue;
            if ( cut.find("#") == std::string::npos )
            {
              std::cout << getTime() << "  -> " << cut << std::endl;
              if ( !fullSelection.empty() ) { fullSelection += " && "; }
              fullSelection += "(" + cut + ")";
            }
            else
            { // Comment, so just print
              std::cout << getTime() << " -> " << cut << std::endl;
            }
          }

        }

      }

    }

  }
  else
  {

    // Decompose a comma seperated list into a vector of strings
    std::vector<std::string> splitcuts;
    boost::split( splitcuts, cuts, boost::is_any_of(",") );

    // Loop over cut sels
    for ( auto& cut : splitcuts )
    {
      if ( cut.empty() ) continue;
      if ( cut.find("#") == std::string::npos )
      {
        std::cout << getTime() << "  -> " << cut << std::endl;
        if ( !fullSelection.empty() ) { fullSelection += " && "; }
        fullSelection += "(" + cut + ")";
      }
      else
      { // Comment, so just print
        std::cout << getTime() << " -> " << cut << std::endl;
      }
    }

  }

  sel = TCut( fullSelection.c_str() );

  return ok;
}
