#!/bin/bash
# This would work in a snakemake type setup, however for now as I want flexibility, I will stick with the other AddTMVAOutput.sh script and do it by hand. Eventually when it's all set up I can put all of the MVA stuff into the main Snakefile (that will be fun)

# This would be good to have in snakemake. Do I want to automate training and choosing an optimal BDT? For now, I think I'll leave that step to do by hand, so snakemake runs will have to pause after the combination, then I'll manually do the MVA training, and then tell snakemake to do the last bit. That means I'll need to change this to just take a single file as input

# This applies the output of the current TMVA classifer to all 32 data files from run 1. It does not do any training of its own 
# format: "RealMagUp_BDhhh_2015_BDKPiPi_DPiPiKSLL.tuples.root:BDKPiPi_DPiPiKSLL_Tuple/BDKPiPiTuple"

infile=$1 # e.g. /r01/lhcb/lovell/b2dkpipi/rootfiles/snaketest/combined/RealMagUp_BDhhh_2018_BDKPiPi_DPiPiKSLL_WS.tuples.root
outfile=$2 # e.g. /r01/lhcb/lovell/b2dkpipi/rootfiles/snaketest/bdt-applied/RealMagUp_BDhhh_2018_BDKPiPi_DPiPiKSLL_WS.tuples.root
outfiledir=${outfile%/*} # This removes everything after the final "/" character, leaving just the path. This step has no subjobs, so all output files will go into the same directory
mkdir -p $outfiledir
tree="BD"

echo "================================================================================================================"
echo "Running $infile"
# This lot just works out some details so everything can be run in one go
# This selects KPiPi (signal)/PiPiPi (not signal) data
if echo "$infile" | grep -q "KPiPi_"; then
	echo "This is KPiPi data"
	signal=true
    tree+="KPiPi_DPiPiKS"
else
	echo "This is PiPiPi data"
	signal=false
    tree+="PiPiPi_DPiPiKS"
fi
# LL or DD - this doesn't require its own nested if when running, as the correct bit is directly appended to the strings run
if echo "$infile" | grep -q "LL"; then
	mode="LL"
    tree+="LL"
else
	mode="DD"
    tree+="DD"
fi
echo "The datatype is $mode"
if echo "$infile" | grep -q "WS"; then
    tree+="_WS"
fi
tree+="_Tuple/BDKPiPiTuple"
echo "================================================================================================================"

# Deciding between KPiPi and PiPiPi, and running the correct one
if [ "$signal" = true ]; then
	# Running KPiPi
	cp TMVAInputs$mode.txt DataSet/output-$mode/weights/
	# the cuts used to be signalcuts$mode.txt, but now that cuts are pre-applied it doesn't matter any more
	echo "TMVAReader.exe --TreeName $tree --Inputs TMVAInputs$mode.txt --InRootFiles $infile:$tree --OutRootFile $outfile --WorkDir DataSet/output-$mode/weights --PreSelCutsFile cuts/signalcutsnew.txt"
else
	# Running PiPiPi - NB if I ever change the M(hhh)<2GeV cut, the 3pi data will need to be changed, as that cut is pre-applied
	cp TMVAInputs3Pi$mode.txt DataSet/output-$mode/weights/
	# The cuts used to be signalcuts3pi$mode.txt, however now cuts are pre-applied this isn't releveant any more
	echo "TMVAReader.exe --TreeName $tree --Inputs TMVAInputs3Pi$mode.txt --InRootFiles $infile:$tree --OutRootFile $outfile --WorkDir DataSet/output-$mode/weights --PreSelCutsFile cuts/signalcutsnew.txt"
fi

# This could do the hadd step for you, but better to do that as a separate snakemake rule I think. The commands would be:
#hadd -ff /r01/lhcb/lovell/b2dkpipi/rootfiles/bdt-applied/v2/all-real-signal-LL.root /r01/lhcb/lovell/b2dkpipi/bdt-applied/v2/Real*KPiPi*LL*
#hadd -ff /r01/lhcb/lovell/b2dkpipi/rootfiles/bdt-applied/v2/all-real-signal-DD.root /r01/lhcb/lovell/b2dkpipi/bdt-applied/v2/Real*KPiPi*DD*

echo "All done!"

exit 0
