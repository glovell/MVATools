
// STL
#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <memory>
#include <algorithm>

// local
#include "MVATraining/ParseOptions.h"
#include "MVATraining/ChainNTuples.h"
#include "MVATraining/TCutFromTextFile.h"
#include "MVATraining/GetTime.h"
#include "MVATraining/DecodeInputs.h"
#include "MVATraining/NTupleReader.h"

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCut.h"
#include "TEntryList.h"
#include "Compression.h"
#include "TSystem.h"
#include "TROOT.h"

// Boost
#include <boost/regex.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/format.hpp"
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/bzip2.hpp>

int main( int argc, char** argv )
{

  // Enable ROOT multi threading
  ROOT::EnableImplicitMT();

  // Options Parser
  ParseOptions options;
  // Define the options for this application
  options.defineOption( "InRootFiles", "in.files",
                        "Input ROOT File list.\n"
                        "Can either be a path to a ROOT file, a comma separated list\n"
                        "of ROOT files, or a text file that contains a list of ROOT\n"
                        "files, with one file per line." );
  options.defineOption( "CutsFile",    "",          "Selection cuts"       );
  options.defineOption( "InputTreeName",    "UNDEFINED", "TTree name"           );
  options.defineOption( "MaxEntries",  "-1",  "Maximum number of filtered entries" );
  options.defineOption( "Variables", "",    "List of variables to write out" );
  options.defineOption( "OutputTextFile",  "data.txt.gz",  "Output file name"   );

  // Parse the arguments
  if ( !options.parse(argc,argv) ) { return 1; }

  // Get the list of input files
  const auto inFiles = options.parameter<std::string>("InRootFiles");

  // Input Tuple name
  const auto inTreeName = options.parameter<std::string>("InputTreeName");

  // Chain the ROOT files
  auto chain = chainRootFiles( inFiles, inTreeName );
  if ( !chain ) { return 1; }
  if ( chain->GetEntries() == 0 )
  {
    std::cerr << getTime() << "ERROR : Empty input chain" << std::endl;
    return 1;
  }

  // Create the TCut for the selection
  TCut sel;
  if ( !createTCut( options.parameter<std::string>("CutsFile"), sel ) )
  { std::cerr << getTime()
              << "ERROR : Problem parsing selection cuts" << std::endl; return 1; }

  // Select entries using the TCut
  std::cout << getTime() << "Scanning the input TChain to select entries ..." << std::endl;
  chain->Draw( ">> elist", sel, "entrylist" );
  auto elist = (TEntryList*) gDirectory->Get("elist");
  if ( !elist )
  { std::cerr << getTime()
              << "ERROR : Problem getting selected entries" << std::endl; return 1; }
  std::cout << getTime() << "  -> Selected " << elist->GetN() << " entries from "
            << chain->GetEntries()
            << " ( " << 100.0 * elist->GetN() / chain->GetEntries() << " % )"
            << std::endl;

  // variables to write out
  const auto VarsFile = options.parameter<std::string>("Variables");
  if ( VarsFile.empty() )
  { std::cerr << getTime() << "ERROR : No variables file defined" << std::endl; 
    return 1; }
  std::ifstream vars( VarsFile.c_str() );
  if ( !vars.is_open() )
  { std::cerr << getTime() << " -> ERROR : Failed to open " << VarsFile << std::endl;
    return 1; }
  std::vector<std::string> variables;
  std::vector<std::string> formulas;
  std::vector<std::string> types;
  std::string var;
  unsigned int maxNameSize{14};
  std::cout << getTime() << "Reading variables to write" << std::endl;
  while ( std::getline(vars,var) )
  {
    if ( !var.empty() && var.find("#") == std::string::npos )
    {
      boost::regex expr("(.*);(.*);(.*)");
      boost::cmatch matches;
      if ( boost::regex_match( var.c_str(), matches, expr ) )
      {
        std::string name = matches[1];
        boost::erase_all(name," ");
        std::string type = matches[2];
        boost::erase_all(type," ");
        std::string form = matches[3];
        boost::erase_all(form," ");
        std::cout << getTime() << " -> " 
                  << type << " " << name << " " << form << std::endl;
        variables.emplace_back( name );
        types    .emplace_back( type );
        formulas .emplace_back( form );
        if ( variables.back().size()+1 > maxNameSize ) 
        { maxNameSize = variables.back().size()+1; }
      }
    }
  }
  
  NTupleReader reader( chain.get(), formulas );
  if ( !reader.isOK() )
  { std::cerr << " -> ERROR : Failed initialise tuple reader" << std::endl; return 1; }

  // maximum number of entries
  const auto maxEntries = options.parameter<Long64_t>("MaxEntries");

  // Output file name
  const auto outFileName = options.parameter<std::string>("OutputTextFile");
  std::cout << getTime() << "Opening output data file '" << outFileName << "'" << std::endl;
  std::ofstream outFile( outFileName.c_str(), std::ios_base::out | std::ios_base::binary );
  if ( !outFile.is_open() )
  { std::cerr << getTime() << " -> ERROR : Failed to open " << outFileName << std::endl;
    return 1; }
  // output stream
  boost::iostreams::filtering_ostream out;
  // add compressor if file extension indicates
  if      ( outFileName.find(".gz") != std::string::npos )
  {
    std::cout << getTime() << " -> Enabling gzip compression" << std::endl;
    out.push(boost::iostreams::gzip_compressor());
  }
  else if ( outFileName.find(".bz2") != std::string::npos )
  {
    std::cout << getTime() << " -> Enabling bzip2 compression" << std::endl;
    out.push(boost::iostreams::bzip2_compressor());
  }
  out.push(outFile);

  // boost format strings
  auto fD = boost::format( "%"+std::to_string(maxNameSize)+".6e" );
  auto fI = boost::format( "%"+std::to_string(maxNameSize)+"i"   );
  auto fS = boost::format( "%"+std::to_string(maxNameSize)+"s"   );

  // write first line legion
  for ( const auto & v : variables ) { out << fS % v << " "; }
  out << std::endl;

  // Loop over the entries and write out those passing the selection
  std::cout << getTime() << "Writing the output data file ..." << std::endl;
  chain->SetEntryList(elist);
  const auto nEntries = ( maxEntries > 0 ? 
                          std::min( elist->GetN(), maxEntries ) :
                          elist->GetN() );
  std::cout << getTime() << "Using " << nEntries << " entries" << std::endl;
  const auto inc = ( nEntries >= 20 ? nEntries/20 : 1 );
  for ( int entry = 0; entry < nEntries; ++entry )
  {

    // get the selected entries
    const auto entryN = chain->GetEntryNumber(entry);
    if ( entryN < 0 ) break;
    chain->GetEntry(entryN);
    if ( (entry+1) % inc == 0 )
    {
      const auto pc = (100.0*((float)(1+entry)/(float)nEntries));
      std::cout << getTime()
                << " -> Read entry " << boost::format("%12i") % (1+entry)
                << " ( " << boost::format("%3.0f") % pc << " % )"
                << std::endl;
    }
   
    // write data to file
    for ( std::size_t i = 0; i < formulas.size(); ++i ) 
    {
      if      ( types[i] == "D" )
      {
        out << fD % reader.variableDouble( formulas[i] ) << " ";
      }
      else if ( types[i] == "F" )
      {
        out << fD % reader.variableFloat( formulas[i] ) << " ";
      }
      else if ( types[i] == "I" )
      {
        out << fI % reader.variableInt( formulas[i] ) << " ";
      }
    }
    out << std::endl;

  }

  std::cout << getTime() << "Done !" << std::endl;

  // return
  return 0;
}
