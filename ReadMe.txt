This package provides a set of applications for training MVA selections using
NeuroBayes and TMVA from standard (DecayTreeTuple) ROOT ntuples. 
The general design is a basic 'command line application with arguments'
approach.

For more details on NeuroBayes see the twiki.

https://twiki.cern.ch/twiki/bin/view/Main/NeuroBayes

or the manuals at

http://neurobayes.phi-t.de/nb_doc/NeuroBayes-HowTo.pdf
http://neurobayes.phi-t.de/nb_doc/HowToCpp.pdf

or, for TMVA,

http://tmva.sourceforge.net

NOTE : Currently NeuroBayes integration in the package is disabled. Only TMVA
       training is currently supported...

TMVA Integration
----------------

NeuroBayes does have a TMVA interface. This is unfortunately not built into the
latest ROOT releases and you have to patch and rebuild ROOT/TMVA
yourself. Details on how to do this are in the above twiki. 

The applications in this package are essentially an alternative way to go about 
providing a consistent ROOT interface to NeuroBayes and TMVA. TMVA of course has
its own ROOT integration, which is very good. This package provides an
alternative, that is interchangable with NeuroBayes.

Build Instructions
------------------

The project can be built using the following commands

 > lb-project-init
 > make install

this will build the applications documented below.

To get a runtime environment then run

 > ./build.x86_64-slc6-gcc49-opt/run bash

Quick Start (NeuroBayes)
------------------------

The sub directory 'examples' includes a couple of shell scripts that provide a
basic example usage of the applications.

 > ./RunNBTeacher.sh

This script runs an example training, followed by a couple basic NeuroBayes
utilities to create a ROOT file and PDF that documents the training performance.

Note that the teacher requires a license at runtime. The package is set up for
lxplus so my recommendation is to run it there.

 > ./AddNBNNOutput.sh

This script uses the training from the previous one, to read in the given ROOT
files, compute the NB output value for each entry, and writes out a new ROOT file
that is identical to the input, but includes the NB output variable as a new
branch. The branch name can be figured via the command line option 
"--ANNOutputBranchName". The default name is "NBANNOut".

Quick Start (TMVA)
------------------

The sub directory 'examples' includes a couple of shell scripts that provide a
basic example usage of the applications.

 > ./RunTMVATeacher.sh

This script runs an example training with TMVA.

 > ./AddTMVANNOutput.sh

This script uses the training from the previous one, to read in the given ROOT
files, compute the TMVA output value for each entry, and writes out a new ROOT file
that is identical to the input, but includes the TMVA output variable as a new
branch. The branch name can be figured via the command line option 
"--ANNOutputBranchName". The default name is "TMVAANNOut".

Data File Specification
-----------------------

The simplest mode of training is to give separate signal and background training
ROOT files, from which all the entries are used. These files are specified using
the options :-

--SignalTrainingFile signal.root:TreeName 
--BckgTrainingFile background.root:TreeName

Where the syntax is <filename>:<treename>

It is though also possible to specify cuts that will be applied to these files,
and in this case only those entries passing the cuts will be used in the
training. This is done by giving the options

--SignalTrainingFile signal-cuts.txt

and/or

--BckgCuts background-cuts.txt

The format of the cuts file is a simple text file.
The cuts can be anything you could write as a TCut object and apply to the
TTree. Each line in the file is interpreted as a single cut (although of course
this can be built however you want, using || or && operators) and the final
selection cut is built by concatenating together all the cuts from each line
with &&.

So for instance if your text file contained

B_P < 1000 || B_P > 2000
B_PT > 10
B_DIRA > 0.9999

the final cut selection would be

( B_P < 1000 || B_P > 2000 ) && ( B_PT > 10 ) && ( B_DIRA > 0.9999 )

Blank lines are ignored, as are lines starting with # which are interpreted as
comments.

The default setting for both the background and signal cuts file options is a
null string, "", which means no cuts are applied.

When cuts are used, it would then for instance be possible to pass the same ROOT
file as the 'signal' and 'background' samples, with the cuts files set as
appropriate to select the correct entries for each category from the file.

Inputs Specification
--------------------

The training step above requires a list of the input variables to use. This is
provided as a simple text file that documents the inputs using a custom
format. The format is each variable is on a separate line, and has a format
like :-

BsTau ; 14 ; B_s0_TAU

The first entry gives the name of the variable.
The second the variable 'type' (specific for NB and TNVA) gives the pre-processor flag, 
which tells the teacher how it should treat the variable. 
For full details you should consult the manual above, however in practise the
main types for NeuroBayes classification are :-

For 'floating point variables' :-

14 - A continuous (float) variable
34 - A continuous (float) variable with a delta function at -999 that signifies 
     some special case, such as 'not defined'
35 - Same as 34, but where the purity versus the given variable has to be 
     monotonous versus the given variable.
94 - Same as 34, but training will not use information on how often you have a 
     delta function in signal and background
95 - Same as 35, but training will not use information on how often you have a 
     delta function in signal and background

In most cases, 14/34 is a good starting bet.

For integer/flags :-

18 - An unordered set of integers, where order of the flags has no meaning.
19 - An ordered set of integers (i.e. 3 is better than 2, and 4 better than 3)

For TMVA, the types are 'F' for floating point numbers, and 'I' for integers.

The thrid part of each line in the inputs text file gives the variable
name. This can either be just the name of a variable in the TTree, or some
function based on them. For example

MinMuIPChi2 ; 14 ; sqrt(TMath::Min(muplus_MINIPCHI2,muminus_MINIPCHI2))

Blank lines in this file are ignored. Also, lines starting with a # are
considered a comment and also ignored.

See examples/NBInputs.txt or examples/TMVAInputs.txt for a full example.

Training parameters
-------------------

A second text file is required in the training, which gives the training tuning
parameters. This file has the format that again there is one setting per line,
and must have the format :-

<NAME> = <VALUE>

for instance

# Training sample sizes. -1 for each means 
# "Use a 1:1 signal:background mixture of the maximum size you can"
NumSignalEntries     = -1
NumBackgroundEntries = -1

sets the training sample sizes.

See examples/training.txt for a full example.

Weighted Training
-----------------

Unless otherwise specified, the teacher will train the network with +1 as the
target value for signal, and -1 as the target for background. Weighted training
is also possible, where an event by event target value (weight) is passed. 

To use this option you just need to specify the Branch name in the TTree to use
as the weight. i.e.

--WeightsBranch "sWeight"

where "sWeight" is the branch name. As above, any function of parameters in the
input tuple file can be given. For instance :-

--WeightsBranch "sWeightA*sWeightB"

When training with weights it is possible to train from a single ROOT file,
unlike with the unweighted training where the signal and background must be
passed in separate ROOT files. In this case simple set the background ROOT file
to "NONE"

 --BckgTrainingFile "NONE"

and only the "Signal" file will be used

Other Utilities
---------------

The package also builds one other application, which has nothing to do with
NeuroBayes or TMVA itself, but just provides a general 'filter' application. 
It reads in a ROOT Tree, applies a filter based on a set of cuts that are
specified in a plain text file, and writes out the selected entries in a new
file.

The application is called FilterNTuple.exe

The format of the cuts file is identical to that in the "Data File
Specification" section above.

Only the entries passing the above cut would be written out to the output ROOT
file.

This application is useful, for instance, in creating the signal and background
sample files, which you can then pass to the teacher.

See the application help for more details.

Application Help
----------------

All the applications have a --help option. e.g.


 > TMVATeacher.exe --help

Available options
-----------------

--SignalTrainingFile <value> ( Default = 'signal.root' )
    Signal sample ROOT file. This file is mandatory.

--BckgTrainingFile <value> ( Default = 'background.root' )
    Background sample ROOT file.
    'NONE' means skip this file, and this
    mode of operation might be useful when using
    the WeightsBranch option.

--TrainingParameters <value> ( Default = 'params.txt' )
    Training parameters text file.

--Inputs <value> ( Default = 'inputs.txt' )
    List of input variables.

--SignalCuts <value> ( Default = '' )
    Text file containing a list of cuts to apply to
    the signal ROOT TTree. Only those entries passing
    the cuts will be presented to the teacher.
    An empty string means no cuts will be applied.

--BckgCuts <value> ( Default = '' )
    Text file containing a list of cuts to apply to
    the background ROOT TTree. Only those entries passing
    the cuts will be presented to the teacher.
    An empty string means no cuts will be applied.

--Spectators <value> ( Default = 'NONE' )
    List of spectator variables.

--Name <value> ( Default = 'TMVATeacher' )
    Application Name

--OutputDir <value> ( Default = 'output' )
    Output Directory

--WeightsBranch <value> ( Default = '' )
    The name of the branch in the input TTree to use as
    the event weight.
    Empty string means use +1 for signal, -1 for background.

-h, --help
    Print the help

Return codes
-----------------

    0     Success
    1     Error




 > TMVAReader.exe --help

Available options
-----------------

--InRootFiles <value> ( Default = 'in.files' )
    Input ROOT File list.
    Can either be a path to a ROOT file, a comma separated list
    of ROOT files, or a text file that contains a list of ROOT
    files, with one file per line.

--OutRootFile <value> ( Default = 'out.root' )
    Output ROOT File name

--Inputs <value> ( Default = 'inputs.txt' )
    List of input variables

--Spectators <value> ( Default = 'NONE' )
    List of spectator variables.

--WorkDir <value> ( Default = 'weights' )
    The XML weights directory

--PreSelCutsFile <value> ( Default = '' )
    PreSelection cuts. An empty string means no cuts.

--TreeName <value> ( Default = 'UNDEFINED' )
    TTree name (including any directories) in the input ROOT file(s).

-h, --help
    Print the help

Return codes
-----------------

    0     Success
    1     Error




 > NBTeacher.exe --help     

Available options
-----------------

--SignalTrainingFile <value> ( Default = 'signal.root' )
    Signal sample ROOT file. This file is mandatory.

--BckgTrainingFile <value> ( Default = 'background.root' )
    Background sample ROOT file.
    'NONE' means skip this file, and this
    mode of operation might be useful when using
    the WeightsBranch option.

--TrainingParameters <value> ( Default = 'params.txt' )
    Training parameters text file.

--Inputs <value> ( Default = 'inputs.txt' )
    List of input variables.

--SignalCuts <value> ( Default = '' )
    Text file containing a list of cuts to apply to
    the signal ROOT TTree. Only those entries passing
    the cuts will be presented to the teacher.
    An empty string means no cuts will be applied.

--BckgCuts <value> ( Default = '' )
    Text file containing a list of cuts to apply to
    the background ROOT TTree. Only those entries passing
    the cuts will be presented to the teacher.
    An empty string means no cuts will be applied.

--OutputExpertiseFile <value> ( Default = 'expert.nb' )
    Output expertise file.

--SignalTreeName <value> ( Default = 'UNDEFINED' )
    Signal TTree name.

--BckgTreeName <value> ( Default = 'UNDEFINED' )
    Background TTree name.

--WeightsBranch <value> ( Default = '' )
    The name of the branch in the input TTree to use as
    the event weight.
    Empty string means use +1 for signal, -1 for background.

-h, --help
    Print the help

Return codes
-----------------

    0     Success
    1     Error




 > NBExpert.exe --help                        

Available options
-----------------

--InRootFiles <value> ( Default = 'in.files' )
    Input ROOT File list.
    Can either be a path to a ROOT file, a comma separated list
    of ROOT files, or a text file that contains a list of ROOT
    files, with one file per line.

--OutRootFile <value> ( Default = 'out.root' )
    Output ROOT File name

--Inputs <value> ( Default = 'inputs.txt' )
    List of input variables

--InputExpertiseFile <value> ( Default = 'expert.nb' )
    The trained network expertise file.

--PreSelCutsFile <value> ( Default = '' )
    PreSelection cuts. An empty string means no cuts.

--TreeName <value> ( Default = 'UNDEFINED' )
    TTree name (including any directories) in the input ROOT file(s).

--ANNOutputBranchName <value> ( Default = 'NBANNOut' )
    The name to give the new Branch in the output TTree that
    contains the ANN output variable.

-h, --help
    Print the help

Return codes
-----------------

    0     Success
    1     Error




 > FilterNTuple.exe --help

Available options
-----------------

--InRootFiles <value> ( Default = 'in.files' )
    Input ROOT File list.
    Can either be a path to a ROOT file, a comma separated list
    of ROOT files, or a text file that contains a list of ROOT
    files, with one file per line.

--OutRootFile <value> ( Default = 'out.root' )
    Output ROOT File name

--CutsFile <value> ( Default = '' )
    Selection cuts

--InputTreeName <value> ( Default = 'UNDEFINED' )
    TTree name

--MaxEntries <value> ( Default = '-1' )
    Maximum number of filtered entries

--AddedVariables <value> ( Default = '' )
    List of variables to add

--OutputTreeName <value> ( Default = '' )
    Output TTree name. Empty means same as input

--CloneInputBranches <value> ( Default = '1' )
    Clone the branches on the input tuple to the output

-h, --help
    Print the help

Return codes
-----------------

    0     Success
    1     Error
