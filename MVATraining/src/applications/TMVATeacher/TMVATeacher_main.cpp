
// STL
#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <map>
#include <vector>
#include <utility>
#include <memory>

// local
#include "MVATraining/ParseOptions.h"
#include "MVATraining/NTupleReader.h"
#include "MVATraining/TCutFromTextFile.h"
#include "MVATraining/DecodeInputs.h"
#include "MVATraining/Parameters.h"
#include "MVATraining/GetTime.h"
#include "MVATraining/ChainNTuples.h"

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TEntryList.h"
#include "TCut.h"
#include "Compression.h"
#include "TSystem.h"
#include "TROOT.h"

// TMVA
#include "TMVA/Config.h"
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/DataLoader.h"

// Boost
#include <boost/regex.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/format.hpp"

// common variables
typedef DecodeInputs<char> Inputs;
Inputs inputs;

double uniRand()
{
  static TRandom rand;
  return rand.Uniform(1.0);
}

bool loadTrainingData( TTree * tree,
                       TMVA::DataLoader * tmvaDLoader,
                       const std::string & weightsBranch,
                       TEntryList * elist,
                       const float tmvaTestFrac,
                       const bool isSignal,
                       const int maxEntries,
                       const double weight = 1.0 )
{
  if ( !tree || !elist ) return false;

  // Make an NTuple reader
  auto _formulas = inputs.formulas();
  if ( !weightsBranch.empty() ) _formulas.push_back(weightsBranch);
  NTupleReader reader(tree,_formulas);
  if ( !reader.isOK() ) { return false; }

  // vector of variables
  std::vector<double> InputVector(inputs.inputs().size());

  // Loop over the selected entries
  tree->SetEntryList(elist);
  const auto nEntries = elist->GetN();
  const auto inc = ( nEntries >= 20 ? nEntries/20 : 1 );
  std::cout << getTime() << " -> Will use " << maxEntries << " entries" << std::endl;
  for ( int entry = 0; entry < nEntries; ++entry )
  {
    // Have we read enough ?
    if ( maxEntries > 0 && entry >= maxEntries )
    {
      std::cout << getTime() << "Read " << maxEntries << " entries -> stop" << std::endl;
      break;
    }

    // get the selected entry
    const auto entryN = tree->GetEntryNumber(entry);
    if ( entryN < 0 ) break;
    tree->GetEntry(entryN);
    if ( (entry+1) % inc == 0 )
    {
      const auto pc = (100.0*((float)(1+entry)/(float)nEntries));
      std::cout << getTime()
                << " -> Read entry " << boost::format("%12i") % (1+entry)
                << " ( " << boost::format("%3.0f") % pc << " % )"
                << std::endl;
    }

    // Fill an input array for the teacher
    for ( std::pair<Inputs::Formulas::const_iterator,int> i(inputs.formulas().begin(),0);
          i.first != inputs.formulas().end(); ++i.first, ++i.second )
    {
      InputVector[i.second] = (double)reader.variable(*i.first);
    }

    // Pass to TMVA
    const bool useForTesting = ( uniRand() < tmvaTestFrac );
    if ( isSignal )
    {
      if ( !useForTesting )
      {
        tmvaDLoader->AddSignalTrainingEvent( InputVector, weight );
      }
      else
      {
        tmvaDLoader->AddSignalTestEvent    ( InputVector, weight );
      }
    }
    else
    {
      if ( !useForTesting )
      {
        tmvaDLoader->AddBackgroundTrainingEvent( InputVector, weight );
      }
      else
      {
        tmvaDLoader->AddBackgroundTestEvent    ( InputVector, weight );
      }
    }

  }

  return true;
}

int main( int argc, char** argv )
{

  // Enable ROOT multi threading
  ROOT::EnableImplicitMT();

  // Options Parser
  ParseOptions options;
  // Define the options for this application
  options.defineOption( "SignalTrainingFile",  "signal.root",
                        "Signal sample ROOT file. This file is mandatory." );
  options.defineOption( "BckgTrainingFile",    "background.root",
                        "Background sample ROOT file.\n"
                        "'NONE' means skip this file, and this\n"
                        "mode of operation might be useful when using\n"
                        "the WeightsBranch option." );
  options.defineOption( "TrainingParameters",  "params.txt",
                        "Training parameters text file." );
  options.defineOption( "Inputs",              "inputs.txt",
                        "List of input variables."  );
  options.defineOption( "SignalCuts", "",
                        "Text file containing a list of cuts to apply to\n"
                        "the signal ROOT TTree. Only those entries passing\n"
                        "the cuts will be presented to the teacher.\n"
                        "An empty string means no cuts will be applied." );
  options.defineOption( "BckgCuts", "",
                        "Text file containing a list of cuts to apply to\n"
                        "the background ROOT TTree. Only those entries passing\n"
                        "the cuts will be presented to the teacher.\n"
                        "An empty string means no cuts will be applied." );
  options.defineOption( "Spectators", "NONE",
                        "List of spectator variables."  );
  options.defineOption( "Name", "TMVATeacher", "Application Name" );
  options.defineOption( "OutputDir", "output", "Output Directory" );
  options.defineOption( "WeightsBranch", "",
                        "The name of the branch in the input TTree to use as\n"
                        "the event weight.\n"
                        "Empty string means use +1 for signal, -1 for background." );
  // Parse the arguments
  if ( !options.parse(argc,argv) ) { return 1; }

  // Training parameters
  Parameters params;
  if ( !params.decode(options.parameter<std::string>("TrainingParameters")) ) return 1;

  // Fraction of the data to use for testing
  const auto tmvaTestFrac = params.getParam<float>("TMVATESTFRAC",0.2);

  // Training name
  const auto jobName = options.parameter<std::string>("Name");

  // output dir
  const auto outDir = options.parameter<std::string>("OutputDir");

  // TMVA ROOT file
  const std::string tmvaHistoFileName = outDir + "/" + jobName + "-TMVA.root";
  std::unique_ptr<TFile> tmvaHistoFile( TFile::Open( tmvaHistoFileName.c_str(), "RECREATE" ) );
  if ( !tmvaHistoFile )
  {
    std::cerr << getTime() << "Failed to open TMVA output ROOT file "
              << tmvaHistoFileName << std::endl;
    return 1;
  }
  tmvaHistoFile->SetCompressionSettings( ROOT::CompressionSettings(ROOT::kLZMA,4) );
  tmvaHistoFile->cd((tmvaHistoFileName+":/").c_str());

  // TMVA factory
  auto tmvaFactory =
    std::make_unique<TMVA::Factory>( jobName.c_str(),
                                     tmvaHistoFile.get(),
                                     "V:!Silent:!Color:!DrawProgressBar:AnalysisType=Classification" );

  // data loader
  auto tmvaLoader = std::make_unique<TMVA::DataLoader>( "DataSet" );
  // Define the data sets for signal and background
  tmvaLoader->GetDataSetInfo().AddClass("Signal");     // adds class 1
  tmvaLoader->GetDataSetInfo().AddClass("Background"); // adds class 0
  
  // Set some general options
  // these no longer work properly with the new TMVA. To be fixed..
  (TMVA::gConfig().GetIONames()).fWeightFileDir = (outDir+"/weights").c_str();
  (TMVA::gConfig().GetVariablePlotting()).fMaxNumOfAllowedVariablesForScatterPlots = 1000;

  // Read in list of input variables
  std::cout << getTime() << "Reading input variables" << std::endl;
  if ( !inputs.decode(options.parameter<std::string>("Inputs")) ) return 1;

  // Add training variables
  for ( const auto& input : inputs.inputs() )
  {
    char type;
    if ( !inputs.nodeType( input, type ) ) { return 1; }
    tmvaLoader->AddVariable( input.c_str(), type );
  }

  // Add spectator variables
  const auto spectatorsFile = options.parameter<std::string>("Spectators");
  if ( "NONE" != spectatorsFile )
  {
    std::cout << getTime() << "Reading spectator variables" << std::endl;
    Inputs spectators( spectatorsFile );
    if ( !spectators.status() ) { return 1; }
    for ( const auto& input : spectators.inputs() )
    {
      if ( std::find( inputs.inputs().begin(),
                      inputs.inputs().end(),
                      input ) == inputs.inputs().end() )
      {
        char type;
        if ( !spectators.nodeType( input, type ) ) { return 1; }
        std::string formula;
        if ( !spectators.nodeFormula( input, formula ) ) { return 1; }
        tmvaLoader->AddSpectator( input.c_str(), formula.c_str(), type );
      }
      else
      {
        std::cout << getTime()
                  << "Spectator variable '" << input << "' is an input -> skipping"
                  << std::endl;
      }
    }
  }
  else
  {
    std::cout << getTime() << "No spectator variables" << std::endl;
  }

  // Create the TCut for the signal
  TCut selSignal;
  std::cout << getTime() << "Defining signal cuts " << std::endl;
  if ( !createTCut( options.parameter<std::string>("SignalCuts"), selSignal ) )
  { std::cerr << getTime() << "ERROR : Problem parsing signal selection cuts" << std::endl;
    return 1; }

  // Create the TCut for the background
  TCut selBckg;
  std::cout << getTime() << "Defining background cuts " << std::endl;
  if ( !createTCut( options.parameter<std::string>("BckgCuts"), selBckg ) )
  { std::cerr << getTime() << "ERROR : Problem parsing background selection cuts" << std::endl;
    return 1; }

  // Get the weights branch name
  const auto weightsBranch = options.parameter<std::string>("WeightsBranch");
  if ( !weightsBranch.empty() )
  { std::cout << getTime() << "Using weighted training, weights from Branch '" << weightsBranch
              << "'" << std::endl; }
  else
  { std::cout << getTime() << "Using +1 as target value for signal, 0 for background"
              << std::endl; }

  // signal
  const auto signalFileName = options.parameter<std::string>("SignalTrainingFile");
  std::cout << getTime()
            << "Opening training signal     ROOT file " << signalFileName
            << std::endl;
  auto signalTree = chainRootFiles(signalFileName);
  if ( !signalTree.get() ) { return 1; }

  // background
  const auto bkgFileName = options.parameter<std::string>("BckgTrainingFile");
  std::unique_ptr<TChain> bckgTree(nullptr);
  if ( "NONE" != bkgFileName )
  {
    std::cout << getTime()
              << "Opening training background ROOT file " << bkgFileName
              << std::endl;
    bckgTree = chainRootFiles(bkgFileName);
    if ( !bckgTree.get() ) { return 1; }
  }
  else
  {
    if ( weightsBranch.empty() )
    {
      std::cerr << getTime()
                << "ERROR : Only signal file given, but --WeightsBranch is not set" << std::endl;
      std::cerr << getTime()
                << "      : Training from a single file only makes sense with weighted training"
                << std::endl;
      return 1;
    }
  }

  // Training data sizes
  auto nMaxSignal     = params.getParam<int>("NumSignalEntries",-1);
  auto nMaxBackground = params.getParam<int>("NumBackgroundEntries",-1);
  const auto reweight = params.getParam<std::string>("ReweightForOneToOne","yes");

  // Go back to TMVA ROOT file
  tmvaHistoFile->cd();

  // Selecting signal entries
  std::cout << getTime()
            << "Scanning the input TChain to select signal entries ..."
            << std::endl;
  signalTree->Draw( ">> elist_sig", selSignal, "entrylist" );
  TEntryList * elist_sig = (TEntryList*) gDirectory->Get("elist_sig");
  if ( !elist_sig )
  { std::cerr << getTime() << "ERROR : Problem getting selected signal entries" << std::endl;
    return 1; }
  std::cout << getTime() << "  -> Selected " << elist_sig->GetN() << " entries from "
            << signalTree->GetEntries()
            << " ( " << 100.0 * elist_sig->GetN() / signalTree->GetEntries() << " % )"
            << std::endl;
  if ( elist_sig->GetN() == 0 )
  {
    std::cerr << getTime() << "No signal events selected" << std::endl;
    return 1;
  }

  TEntryList * elist_bkg = nullptr;
  if ( bckgTree )
  {
    // Selecting background entries
    std::cout << getTime()
              << "Scanning the input TChain to select background entries ..."
              << std::endl;
    bckgTree->Draw( ">> elist_bkg", selBckg, "entrylist" );
    elist_bkg = (TEntryList*) gDirectory->Get("elist_bkg");
    if ( !elist_bkg )
    { std::cerr << getTime() << "ERROR : Problem getting selected background entries"
                << std::endl;
      return 1; }
    std::cout << getTime() << "  -> Selected " << elist_bkg->GetN() << " entries from "
              << bckgTree->GetEntries()
              << " ( " << 100.0 * elist_bkg->GetN() / bckgTree->GetEntries() << " % )"
              << std::endl;
    if ( elist_bkg->GetN() == 0 )
    {
      std::cerr << getTime() << "No background events selected" << std::endl;
      return 1;
    }
  }

  // signal and background weights
  double sigWeight(1.0), bckWeight(1.0);

  if ( elist_bkg && elist_sig )
  {
    // if we have not been given specific numbers for training sizes, use a 1:1 mix
    if ( nMaxSignal < 0 && nMaxBackground < 0 )
    {
      if ( "yes" == reweight )
      {
        const double meanN = ( elist_sig->GetN() + elist_bkg->GetN() ) / 2.0;
        sigWeight = meanN / elist_sig->GetN();
        bckWeight = meanN / elist_bkg->GetN();
        nMaxSignal     = elist_sig->GetN();
        nMaxBackground = elist_bkg->GetN();
      }
      else
      {
        // determine the maximum amount of data we can use for a 1:1 mix
        const auto commonEntries = std::min( elist_sig->GetN(),
                                             elist_bkg->GetN() );
        nMaxSignal = nMaxBackground = commonEntries;
      }
    }
    else 
    {
      if ( "yes" == reweight )
      {
        const auto nSig = std::min( (Long64_t)nMaxSignal,     elist_sig->GetN() );
        const auto nBck = std::min( (Long64_t)nMaxBackground, elist_bkg->GetN() );
        const auto meanN = ( nSig + nBck ) / 2.0;
        nMaxSignal     = nSig;
        nMaxBackground = nBck;
        sigWeight = meanN / nMaxSignal;
        bckWeight = meanN / nMaxBackground;
      }
      else
      {
        // determine the maximum amount of data we can use for a 1:1 mix
        const auto nSig = std::min( (Long64_t)nMaxSignal,     elist_sig->GetN() );
        const auto nBck = std::min( (Long64_t)nMaxBackground, elist_bkg->GetN() );
        const auto commonEntries = std::min( nSig, nBck );
        nMaxSignal = nMaxBackground = commonEntries;
      }
    }
  }
  else
  {
    if ( nMaxSignal < 0 )
    {
      nMaxSignal = nMaxBackground = signalTree->GetEntries();
    }
  }

  std::cout << getTime()
            << "Will use " << nMaxSignal << " signal entries with weight = "
            << sigWeight << std::endl;
  std::cout << getTime()
            << "Will use " << nMaxBackground << " background entries with weight = " 
            << bckWeight << std::endl;
  
  // Load the training data
  std::cout << getTime() << "Reading in signal data" << std::endl;
  loadTrainingData( signalTree->GetTree(), tmvaLoader.get(), weightsBranch, elist_sig,
                    tmvaTestFrac, true,  nMaxSignal, sigWeight     );
  std::cout << getTime() << "Reading in background data" << std::endl;
  loadTrainingData( bckgTree->GetTree(),   tmvaLoader.get(), weightsBranch, elist_bkg,
                    tmvaTestFrac, false, nMaxBackground, bckWeight );

  // Go back to TMVA ROOT file
  tmvaHistoFile->cd();

  // Prepare the data
  tmvaLoader->PrepareTrainingAndTestTree( TCut(""),
                                          "nTrain_Signal=0:nTrain_Background=0"
                                          ":SplitMode=Random:NormMode=None:V" );
  
  // Classifiers to run
  const std::string names = params.getParam<std::string>("TMVANAMES","NAME");
  std::vector<std::string> classifers;
  boost::split( classifers, names, boost::is_any_of(",") );

  // Loop of the classifers to train
  for ( const auto& classifer : classifers )
  {
    // Get classier method
    const auto tmvaMethod = params.getParam<std::string>(classifer+":TMVAMethod","UNDEFINED");

    // The TMVA config string
    auto tmvaConfig = params.getParam<std::string>(classifer+":TMVAConfig","UNDEFINED");

    // Method specific options ...
    if ( "MLP" == tmvaMethod )
    {
      // Number of inputs and hidden nodes
      const auto nvar = inputs.inputs().size();

      // Layer 2 scale factor
      const auto layerTwoScale = params.getParam<double>(classifer+":MLPLayerTwoScale",1.2);

      // Compute # of hidden nodes
      const auto nHiddenNodes = (int)(layerTwoScale*(nvar+1));
      std::cout << getTime()
                << "#Inputs=" << nvar << " #HiddenNodes=" << nHiddenNodes
                << std::endl;

      // construct options string for TMVA
      std::ostringstream opts;
      opts << ":HiddenLayers=" << nHiddenNodes;
      tmvaConfig += opts.str();
    }

    // Book the method
    std::cout << getTime() << "Adding Classifier " << classifer
              << " Method=" << tmvaMethod << " Config='" << tmvaConfig << "'"
              << std::endl;
    tmvaFactory->BookMethod( tmvaLoader.get(),
                             TMVA::Types::Instance().GetMethodType(tmvaMethod),
                             classifer.c_str(), tmvaConfig.c_str() );
  }

  // EDIT: add in optimise all methods (only works for BDTs I think)
  std::cout << getTime() << "Optimsing Methods..." << std::endl;
  tmvaFactory->OptimizeAllMethods("ROCIntegral","FitGA"); // These are lifted with no motivation from the examples

  // Train MVAs using the set of training events
  std::cout << getTime() << "Starting Training..." << std::endl;
  tmvaFactory->TrainAllMethods();

  // Evaluate all MVAs using the set of test events
  std::cout << getTime() << "Starting Testing..." << std::endl;
  tmvaFactory->TestAllMethods();

  // Evaluate and compare performance of all configured MVAs
  std::cout << getTime() << "Starting Evaluation..." << std::endl;
  tmvaFactory->EvaluateAllMethods();

  // Cleanup
  std::cout << getTime() << "... Complete :)" << std::endl;

  // return
  return 0;
}
