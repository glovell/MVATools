
# Blank lines and lines like this starting with # are ignored

# Training sample sizes. -1 for each means 
# "Use a 1:1 signal:background mixture of the maximum size you can"
NumSignalEntries     = 1000
NumBackgroundEntries = 1000

# This line gives the scale factor to compute the number of nodes in the
# hidden layer. So 1.3 means 'use int(<number of inputs>*1.3) hidden nodes'. 
# In my experience the number of nodes in the hidden layer should be 
# 'a bit bigger' than the number of inputs, hence the above system, but in 
# general the training is not heavily dependent on this.
LayerTwoScale = 1.3

# Neurobayes parameters
# See the NeuroBayes manual for more details
NB_DEF_TASK      = CLA
NB_DEF_MOM       = 0.0
NB_DEF_REG       = ALL
NB_DEF_LOSS      = ENTROPY
NB_DEF_METHOD    = BFGS
NB_DEF_SHAPE     = DIAG
NB_DEF_LEARNDIAG = 1
NB_DEF_PRE       = 612
