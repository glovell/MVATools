
// STL
#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <map>
#include <vector>
#include <utility>

// local
#include "MVATraining/ParseOptions.h"
#include "MVATraining/NTupleReader.h"
#include "MVATraining/TCutFromTextFile.h"
#include "MVATraining/DecodeInputs.h"
#include "MVATraining/Parameters.h"
#include "MVATraining/GetTime.h"

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TEntryList.h"
#include "TCut.h"

// NeuroBayes
#include "NeuroBayesTeacher.hh"

// Boost
#include <boost/regex.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"

// common variables
typedef DecodeInputs<int> Inputs;
Inputs inputs;
Parameters params;

TTree * loadTree( TFile * file,
                  const std::string & tupleName )
{
  file->cd();
  TTree * tree = (TTree*)gDirectory->Get( tupleName.c_str() );
  if ( !tree )
  {
    std::cerr << "ERROR : Failed to load TTree" << std::endl; return false;
  }
  return tree;
}

bool loadTrainingData( TTree * tree,
                       NeuroBayesTeacher * teacher,
                       const std::string & weightsBranch,
                       TEntryList * elist,
                       const bool isSignal,
                       const int maxEntries )
{
  if ( !tree || !elist ) return false;

  // Make an NTuple reader
  Inputs::Formulas _formulas = inputs.formulas();
  if ( !weightsBranch.empty() ) _formulas.push_back(weightsBranch);
  NTupleReader reader(tree,_formulas);
  if ( !reader.isOK() ) { return false; }

  // Array for variables to pass to NB
  float InputArray[inputs.inputs().size()];

  // Loop over the selected entries
  tree->SetEntryList(elist);
  const int nEntries = elist->GetN();
  const int inc = ( nEntries >= 20 ? nEntries/20 : 1 );
  std::cout << getTime() << " -> Will use " << maxEntries << " entries" << std::endl;
  for ( int entry = 0; entry < nEntries; ++entry )
  {
    // Have we read enough ?
    if ( maxEntries > 0 && entry >= maxEntries )
    {
      std::cout << getTime() << "Read " << maxEntries << " entries -> stop" << std::endl;
      break;
    }

    // get the selected entry
    const int entryN = tree->GetEntryNumber(entry);
    if ( entryN < 0 ) break;
    tree->GetEntry(entryN);
    if ( (entry+1) % inc == 0 )
    {
      std::cout << getTime() << " -> Read entry " << (1+entry)
                << " ( " << 100.0*((float)(1+entry)/(float)nEntries) << " % )"
                << std::endl;
    }

    // Fill an input array for the teacher
    for ( std::pair<Inputs::Formulas::const_iterator,int> i(inputs.formulas().begin(),0);
          i.first != inputs.formulas().end(); ++i.first, ++i.second )
    {
      InputArray[i.second] = (float)reader.variable(*i.first);
    }

    // Get the target value
    float targetValue(0);
    if ( !weightsBranch.empty() )
    {
      targetValue = (float)reader.variable(weightsBranch);
    }
    else
    {
      targetValue = ( isSignal ? 1.0 : -1.0 );
    }

    // Pass to teacher
    teacher->SetTarget    ( targetValue                        );
    teacher->SetNextInput ( inputs.inputs().size(), InputArray );

  }

  return true;
}

int main( int argc, char** argv )
{

  // Options Parser
  ParseOptions options;
  // Define the options for this application
  options.defineOption( "SignalTrainingFile",  "signal.root",
                        "Signal sample ROOT file. This file is mandatory." );
  options.defineOption( "BckgTrainingFile",    "background.root",
                        "Background sample ROOT file.\n"
                        "'NONE' means skip this file, and this\n"
                        "mode of operation might be useful when using\n"
                        "the WeightsBranch option." );
  options.defineOption( "TrainingParameters",  "params.txt",
                        "Training parameters text file." );
  options.defineOption( "Inputs",              "inputs.txt",
                        "List of input variables."  );
  options.defineOption( "SignalCuts", "",
                        "Text file containing a list of cuts to apply to\n"
                        "the signal ROOT TTree. Only those entries passing\n"
                        "the cuts will be presented to the teacher.\n"
                        "An empty string means no cuts will be applied." );
  options.defineOption( "BckgCuts", "",
                        "Text file containing a list of cuts to apply to\n"
                        "the background ROOT TTree. Only those entries passing\n"
                        "the cuts will be presented to the teacher.\n"
                        "An empty string means no cuts will be applied." );
  options.defineOption( "OutputExpertiseFile", "expert.nb",
                        "Output expertise file." );
  options.defineOption( "SignalTreeName",    "UNDEFINED",
                        "Signal TTree name."            );
  options.defineOption( "BckgTreeName",      "UNDEFINED",
                        "Background TTree name."            );
  options.defineOption( "WeightsBranch", "",
                        "The name of the branch in the input TTree to use as\n"
                        "the event weight.\n"
                        "Empty string means use +1 for signal, -1 for background." );
  // Parse the arguments
  if ( !options.parse(argc,argv) ) { return 1; }

  // Read in list of input variables
  if ( !inputs.decode(options.parameter<std::string>("Inputs")) ) return 1;

  // Training parameters
  if ( !params.decode(options.parameter<std::string>("TrainingParameters")) ) return 1;

  // Layer 2 scale factor
  const double layerTwoScale = params.getParam<double>("LayerTwoScale",1.3);

  // Number of inputs and hidden nodes
  const int nvar         = inputs.inputs().size(); // number of input variables
  const int nHiddenNodes = (int)(layerTwoScale*(nvar+1));
  std::cout << getTime() << "Input layer has " << nvar+1 << " nodes, hidden layer has "
            << nHiddenNodes << " nodes" << std::endl;

  // Get the NB teacher
  NeuroBayesTeacher * teacher = NeuroBayesTeacher::Instance();

  // teacher parameters
  teacher->NB_DEF_NODE1(nvar+1);       // nodes in input layer, leave this alone! (+1 is correct...)
  teacher->NB_DEF_NODE2(nHiddenNodes); // nodes in hidden layer, you may play with this one
  teacher->NB_DEF_NODE3(1);            // nodes in output layer
  teacher->NB_DEF_TASK   ( params.getParam<std::string>("NB_DEF_TASK","CLA").c_str()     );
  teacher->NB_DEF_SHAPE  ( params.getParam<std::string>("NB_DEF_SHAPE","DIAG").c_str()   );
  teacher->NB_DEF_REG    ( params.getParam<std::string>("NB_DEF_REG","ALL").c_str()      );
  teacher->NB_DEF_LOSS   ( params.getParam<std::string>("NB_DEF_LOSS","ENTROPY").c_str() );
  teacher->NB_DEF_METHOD ( params.getParam<std::string>("NB_DEF_METHOD","BFGS").c_str()  );
  teacher->NB_DEF_LEARNDIAG ( params.getParam<int>("NB_DEF_LEARNDIAG",1) );
  teacher->NB_DEF_PRE       ( params.getParam<int>("NB_DEF_PRE",621)     );
  teacher->NB_DEF_MOM       ( params.getParam<float>("NB_DEF_MOM",0.0f)  );

  // output expertise file
  const std::string paramFileName = options.parameter<std::string>("OutputExpertiseFile");
  teacher->SetOutputFile( paramFileName.c_str() );
  std::cout << getTime() << "Writing expertise file " << paramFileName << std::endl;

  // Set the variable prepro-flags
  int i(0);
  for ( Inputs::Inputs::const_iterator iIn = inputs.inputs().begin();
        iIn != inputs.inputs().end(); ++iIn, ++i )
  {
    int type(0);
    inputs.nodeType( *iIn, type );
    if ( !inputs.nodeType( *iIn, type ) ) { return 1; }
    teacher->SetIndividualPreproFlag(i,type);
  }

  // Create the TCut for the signal
  TCut selSignal;
  std::cout << getTime() << "Defining signal cuts " << std::endl;
  if ( !createTCut( options.parameter<std::string>("SignalCuts"), selSignal ) )
  { std::cerr << "ERROR : Problem parsing signal selection cuts" << std::endl; 
    return 1; }

  // Create the TCut for the background
  TCut selBckg;
  std::cout << getTime() << "Defining background cuts " << std::endl;
  if ( !createTCut( options.parameter<std::string>("BckgCuts"), selBckg ) )
  { std::cerr << "ERROR : Problem parsing background selection cuts" << std::endl;
    return 1; }

  // Get the weights branch name
  const std::string weightsBranch = options.parameter<std::string>("WeightsBranch");
  if ( !weightsBranch.empty() )
  { std::cout << getTime() << "Using weighted training, weights from Branch '" << weightsBranch
              << "'" << std::endl; }
  else
  { std::cout << getTime() << "Using +1 as target value for signal, -1 for background"
              << std::endl; }
 
  // the name of the TTrees
  const std::string signalTreeName = options.parameter<std::string>("SignalTreeName");
  const std::string bkgTreeName    = options.parameter<std::string>("BckgTreeName");

  // signal
  const std::string signalFileName = options.parameter<std::string>("SignalTrainingFile");
  std::cout << getTime() << "Opening training signal     ROOT file " << signalFileName << std::endl;
  TFile * signalFile = TFile::Open( signalFileName.c_str() );
  if ( !signalFile ) { std::cerr << " -> ERROR : Failed to open" << std::endl; return 1; }
  TTree * signalTree = loadTree( signalFile, signalTreeName );

  // background
  const std::string bkgFileName = options.parameter<std::string>("BckgTrainingFile");
  TFile * bckgFile(NULL);
  TTree * bckgTree(NULL);
  if ( "NONE" != bkgFileName )
  {
    std::cout << getTime() << "Opening training background ROOT file " << bkgFileName << std::endl;
    bckgFile = TFile::Open( bkgFileName.c_str() );
    if ( !bckgFile ) { std::cerr << " -> ERROR : Failed to open" << std::endl; return 1; }
    bckgTree = loadTree( bckgFile, bkgTreeName );
  }
  else
  {
    if ( weightsBranch.empty() )
    {
      std::cerr << "ERROR : Only signal file given, but --WeightsBranch is not set" << std::endl;
      std::cerr << "      : Training from a single file only makes sense with weighted training" 
                << std::endl;
      return 1;
    }
  }

  // Training data sizes
  int nMaxSignal     = params.getParam<int>("NumSignalEntries",-1);
  int nMaxBackground = params.getParam<int>("NumBackgroundEntries",-1);

  // Selecting signal entries
  std::cout << getTime() << "Scanning the input TChain to select signal entries ..." << std::endl;
  signalTree->Draw( ">> elist_sig", selSignal, "entrylist" );
  TEntryList * elist_sig = (TEntryList*) gDirectory->Get("elist_sig");
  if ( !elist_sig )
  { std::cerr << "ERROR : Problem getting selected signal entries" << std::endl; return 1; }
  std::cout << getTime() << "  -> Selected " << elist_sig->GetN() << " signal entries from " 
            << signalTree->GetEntries() 
            << " ( " << 100.0 * elist_sig->GetN() / signalTree->GetEntries() << " % )"
            << std::endl;

  TEntryList * elist_bkg = NULL;
  if ( bckgTree )
  {
    // Selecting background entries
    std::cout << getTime() << "Scanning the input TChain to select background entries ..." << std::endl;
    bckgTree->Draw( ">> elist_bkg", selBckg, "entrylist" );
    elist_bkg = (TEntryList*) gDirectory->Get("elist_bkg");
    if ( !elist_bkg )
    { std::cerr << "ERROR : Problem getting selected background entries" << std::endl; return 1; }
    std::cout << getTime() << "  -> Selected " << elist_bkg->GetN() << " signal entries from " 
              << bckgTree->GetEntries() 
              << " ( " << 100.0 * elist_bkg->GetN() / bckgTree->GetEntries() << " % )"
              << std::endl;
  }

  // if we have not been given specific numbers for training sizes, use a 1:1 mix
  if ( elist_bkg && elist_sig )
  {
    if ( nMaxSignal < 0 && nMaxBackground < 0 )
    {
      // determine the maximum amount of data we can use for a 1:1 mix
      const int commonEntries = std::min( elist_sig->GetN(),
                                          elist_bkg->GetN() );
      nMaxSignal = nMaxBackground = commonEntries;
      std::cout << getTime() << "Set training sample sizes to common " << commonEntries << std::endl;
    }
  }
  else
  {
    if ( nMaxSignal < 0 )
    {
      nMaxSignal = nMaxBackground = signalTree->GetEntries();
      std::cout << getTime() << "Set training sample sizes to " << nMaxSignal << std::endl;
    }
  }

  // Load the training data
  std::cout << getTime() << "Reading in signal data" << std::endl;
  loadTrainingData( signalTree, teacher, weightsBranch, elist_sig, true,  nMaxSignal     );
  std::cout << getTime() << "Reading in background data" << std::endl;
  loadTrainingData( bckgTree,   teacher, weightsBranch, elist_bkg, false, nMaxBackground );
  
  // Close the training data files
  signalTree->Delete();
  signalFile->Close();
  if ( bckgTree ) bckgTree->Delete();
  if ( bckgFile ) bckgFile->Close();

  // Train !
  std::cout << getTime() << "Starting Training." << std::endl;
  teacher->TrainNet();
  std::cout << getTime() << "Training Complete :)" << std::endl;

  // return
  return 0;
}
