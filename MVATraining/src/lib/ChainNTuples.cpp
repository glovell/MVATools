
// STL
#include <fstream>
#include <iostream>
#include <string>

// boost
#include <boost/algorithm/string.hpp>

// Local
#include "MVATraining/ChainNTuples.h"
#include "MVATraining/GetTime.h"

std::unique_ptr<TChain>
TChainCreator::chainRootFiles( const std::string & rootFilesName,
                               const std::string & treeName )
{
  std::cout << getTime() << "Chaining input ROOT files" << std::endl;

  // make a new chain
  auto chain = std::make_unique<TChain>();

  // set tree name
  m_treeName = treeName;

  bool OK = true;

  // Do we have a text file listing ROOT files, or a ROOT file name itself
  if ( rootFilesName.find(".root") != std::string::npos )
  {

    // Decompose a comma seperated list into a vector of strings
    std::vector<std::string> files;
    boost::split( files, rootFilesName, boost::is_any_of(",") );

    // loop over files
    for ( const auto & F : files ) { OK &= addFile( chain.get(), F ); }

  }
  else if ( rootFilesName.find(".txt") != std::string::npos )
  {

    // Open the test file with the ROOT file names
    std::ifstream rootFiles( rootFilesName.c_str() );

    // If OK, process
    if ( !rootFiles.is_open() )
    {
      std::cerr << getTime() << "Failed to open " << rootFilesName << std::endl;
    }
    else
    {
      // add each file
      std::string file;
      while ( std::getline(rootFiles,file) )
      {
        if ( !file.empty() && file.find("#") == std::string::npos )
        {
          OK &= addFile( chain.get(), file );
        }
      }
    }

  }
  else
  {
    std::cerr << getTime() << "Failed to understand " << rootFilesName << std::endl;
    OK = false;
  }

  if ( !OK ) { chain.reset(nullptr); }

  return chain;
}

bool TChainCreator::addFile( TChain * tree, const std::string & rootFileName )
{
  bool OK = true;
  std::cout << getTime() << " -> " << rootFileName << std::endl;
  std::vector<std::string> info;
  boost::split( info, rootFileName, boost::is_any_of(":") );
  if ( info.size() == 2 )
  {
    tree->AddFile( info[0].c_str(), TChain::kBigNumber, info[1].c_str() );
  }
  else if ( !m_treeName.empty() )
  {
    std::cout << getTime()
              << "  -> " << "Using forced Tree name " << m_treeName << std::endl;
    tree->AddFile( info[0].c_str(), TChain::kBigNumber, m_treeName.c_str() );
  }
  else
  {
    std::cerr << getTime()
              << "Cannot decode file:tuple string " << rootFileName << std::endl;
    OK = false;
  }
  return OK;
}
