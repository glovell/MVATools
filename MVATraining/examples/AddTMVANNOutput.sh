#!/bin/bash

# Add the NB Output variable for the above training to various input ROOT files

# Data files
export SIGNALFILE="signal.root:B2MuMuTuple/DecayTree"
export BCKGRDFILE="background.root:B2MuMuTuple/DecayTree"

cp TMVAInputs.txt DataSet/output/weights/

rm -f signalWithNBoutput.root
TMVAReader.exe --TreeName "B2MuMuTuple/DecayTree" --Inputs TMVAInputs.txt --InRootFiles $SIGNALFILE --OutRootFile signalWithTMVAoutput.root --WorkDir DataSet/output/weights

rm -f backgroundWithNBoutput.root
TMVAReader.exe --TreeName "B2MuMuTuple/DecayTree" --Inputs TMVAInputs.txt --InRootFiles $BCKGRDFILE --OutRootFile backgroundWithTMVAoutput.root --WorkDir DataSet/output/weights

exit 0
