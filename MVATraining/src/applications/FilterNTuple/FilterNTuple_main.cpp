
// STL
#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <memory>

// local
#include "MVATraining/ParseOptions.h"
#include "MVATraining/ChainNTuples.h"
#include "MVATraining/TCutFromTextFile.h"
#include "MVATraining/GetTime.h"
#include "MVATraining/DecodeInputs.h"
#include "MVATraining/NTupleReader.h"

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCut.h"
#include "TEntryList.h"
#include "Compression.h"
#include "TSystem.h"
#include "TROOT.h"

// Boost
#include <boost/regex.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/format.hpp"

template < typename VARTYPE >
class Variable 
{
public:
  Variable( std::string _name, VARTYPE _var,  std::string _form )
    : name( std::move(_name) ), variable( std::make_unique<VARTYPE>(_var) ), formula(std::move(_form)) 
  { }
public:
  std::string name;
  std::unique_ptr<VARTYPE> variable;
  std::string formula;
  using Vector = std::vector<Variable>;
};

int main( int argc, char** argv )
{

  // Enable ROOT multi threading
  ROOT::EnableImplicitMT();

  // Options Parser
  ParseOptions options;
  // Define the options for this application
  options.defineOption( "InRootFiles", "in.files",
                        "Input ROOT File list.\n"
                        "Can either be a path to a ROOT file, a comma separated list\n"
                        "of ROOT files, or a text file that contains a list of ROOT\n"
                        "files, with one file per line." );
  options.defineOption( "OutRootFile", "out.root",  "Output ROOT File name" );
  options.defineOption( "CutsFile",    "",          "Selection cuts"       );
  options.defineOption( "InputTreeName",    "UNDEFINED", "TTree name"           );
  options.defineOption( "MaxEntries",  "-1",  "Maximum number of filtered entries" );
  options.defineOption( "AddedVariables", "",    "List of variables to add" );
  options.defineOption( "OutputTreeName", "", "Output TTree name. Empty means same as input" );
  options.defineOption( "CloneInputBranches", "1", 
                        "Clone the branches on the input tuple to the output" );
  // Parse the arguments
  if ( !options.parse(argc,argv) ) { return 1; }

  // Get the list of input files
  const auto inFiles = options.parameter<std::string>("InRootFiles");

  // Input Tuple name
  const auto inTreeName = options.parameter<std::string>("InputTreeName");

  // Chain the ROOT files
  auto chain = chainRootFiles( inFiles, inTreeName );
  if ( !chain ) { return 1; }
  if ( chain->GetEntries() == 0 )
  {
    std::cerr << getTime() << "ERROR : Empty input chain" << std::endl;
    return 1;
  }

  // Create the TCut for the selection
  TCut sel;
  if ( !createTCut( options.parameter<std::string>("CutsFile"), sel ) )
  { std::cerr << getTime()
              << "ERROR : Problem parsing selection cuts" << std::endl; return 1; }

  // Select entries using the TCut
  std::cout << getTime() << "Scanning the input TChain to select entries ..." << std::endl;
  chain->Draw( ">> elist", sel, "entrylist" );
  auto elist = (TEntryList*) gDirectory->Get("elist");
  if ( !elist )
  { std::cerr << getTime()
              << "ERROR : Problem getting selected entries" << std::endl; return 1; }
  std::cout << getTime() << "  -> Selected " << elist->GetN() << " entries from "
            << chain->GetEntries()
            << " ( " << 100.0 * elist->GetN() / chain->GetEntries() << " % )"
            << std::endl;

  // Open output ROOT file
  const auto outFileName = options.parameter<std::string>("OutRootFile");
  auto outFile = std::unique_ptr<TFile>( TFile::Open(outFileName.c_str(),"recreate") );
  outFile->SetCompressionSettings( chain->GetFile() ?
                                   chain->GetFile()->GetCompressionSettings() :
                                   ROOT::CompressionSettings(ROOT::kLZMA,4) );
  outFile->cd();

  // Output tree name
  auto outTreeName = options.parameter<std::string>("OutputTreeName");
  if ( outTreeName.empty() ) { outTreeName = inTreeName; }
  std::cout << getTime() << "Output TTree name = " << outTreeName << std::endl;
  auto treeN = outTreeName;

  // Do we have directories in the ROOT file to replicate ?
  const auto slash = outTreeName.find_last_of( "/" );
  if ( slash != std::string::npos )
  {
    const auto topDir = outTreeName.substr(0,slash);
    std::cout << getTime() << "Creating ROOT file directory " << topDir << std::endl;
    auto dir = outFile->mkdir( topDir.c_str() );
    if ( !dir )
    { std::cerr << getTime()
                << "ERROR : Failed to create ROOT directory " << topDir
                << std::endl; return 1; }
    dir->cd();
    treeN = outTreeName.substr(slash+1);
  }

  // Make a new tree or clone the input ?
  const auto cloneInTree = options.parameter<bool>("CloneInputBranches");
  std::unique_ptr<TTree> newTree;
  if ( cloneInTree )
  {
    std::cout << getTime()
              << "Will clone all branches from input TTree to output" << std::endl;
    // Clone the TTree structure
    auto newchain = (TChain*)chain->CloneTree(0);
    newTree.reset( newchain ? newchain->GetTree() : nullptr );
    if ( newTree.get() ) { newTree->SetName(treeN.c_str()); }
  }
  else
  {
    std::cout << getTime()
              << "Will NOT clone all branches from input TTree to output" << std::endl;
    newTree = std::make_unique<TTree>(treeN.c_str(),chain->GetTitle());
  }
  if ( !newTree )
  { std::cerr << getTime() << "ERROR : Failed to clone the TChain" << std::endl; return 1; }

  // Add any additional variables
  const auto addedVarsFile = options.parameter<std::string>("AddedVariables");
  Variable<double>::Vector addedDoubles;
  Variable<float>::Vector  addedFloats;
  Variable<bool>::Vector   addedBools;
  Variable<int>::Vector    addedInts;
  std::vector<std::string> formulas;
  if ( !addedVarsFile.empty() )
  {
    std::string var;
    std::ifstream vars( addedVarsFile.c_str() );
    if ( !vars.is_open() )
    { std::cerr << getTime()
                << " -> ERROR : Failed to open " << addedVarsFile << std::endl; return 1; }
    std::cout << getTime() << "Variables to add" << std::endl;
    while ( std::getline(vars,var) )
    {
      if ( !var.empty() && var.find("#") == std::string::npos )
      {
        boost::regex expr("(.*);(.*);(.*)");
        boost::cmatch matches;
        if ( boost::regex_match( var.c_str(), matches, expr ) )
        {
          std::string name = matches[1];
          boost::erase_all(name," ");
          std::string type = matches[2];
          boost::erase_all(type," ");
          std::string form = matches[3];
          boost::erase_all(form," ");
          std::cout << getTime() << " -> Adding Name='" << name
                    << "' Type=" << type << " Formula='" << form << "'"
                    << std::endl;
          if      ( "F" == type ) { addedFloats.emplace_back ( name, 0.0,   form ); }
          else if ( "D" == type ) { addedDoubles.emplace_back( name, 0.0,   form ); }
          else if ( "B" == type ) { addedBools.emplace_back  ( name, false, form ); }
          else if ( "I" == type ) { addedInts.emplace_back   ( name, 0,     form ); }
          else
          {
            std::cerr << getTime()
                      << "Unknown variable type " << type << std::endl;
            return 1;
          }
          formulas.push_back(form);
        }
      }
    }
    // define new Branches
    for (auto & addedDouble : addedDoubles)
    { newTree->Branch( addedDouble.name.c_str(), addedDouble.variable.get() ); }
    for (auto & addedFloat : addedFloats)
    { newTree->Branch( addedFloat.name.c_str(), addedFloat.variable.get() ); }
    for (auto & addedBool : addedBools)
    { newTree->Branch( addedBool.name.c_str(), addedBool.variable.get() ); }
    for (auto & addedInt : addedInts)
    { newTree->Branch( addedInt.name.c_str(), addedInt.variable.get() ); }
  }

  // Make an NTuple reader for new variables
  NTupleReader reader( chain.get(), formulas );
  if ( !reader.isOK() )
  { std::cerr << " -> ERROR : Failed to initialise tuple reader" << std::endl; return 1; }

  // maximum number of entries
  const auto maxEntries = options.parameter<int>("MaxEntries");

  // Loop over the entries and write out those passing the selection
  std::cout << getTime() << "Filling the filtered TTree ..." << std::endl;
  chain->SetEntryList(elist);
  const auto nEntries = elist->GetN();
  const auto inc = ( nEntries >= 20 ? nEntries/20 : 1 );
  for ( int entry = 0; entry < nEntries; ++entry )
  {
    // get the selected entries
    const auto entryN = chain->GetEntryNumber(entry);
    if ( entryN < 0 ) break;
    chain->GetEntry(entryN);
    if ( (entry+1) % inc == 0 )
    {
      const auto pc = (100.0*((float)(1+entry)/(float)nEntries));
      std::cout << getTime()
                << " -> Read entry " << boost::format("%12i") % (1+entry)
                << " ( " << boost::format("%3.0f") % pc << " % )"
                << std::endl;
    }
    // Fill additional vars
    for (auto & addedDouble : addedDoubles)
    { *addedDouble.variable = reader.variableDouble( addedDouble.formula ); }
    for (auto & addedFloat : addedFloats)
    { *addedFloat.variable = reader.variableFloat( addedFloat.formula ); }
    for (auto & addedBool : addedBools)
    { *addedBool.variable = reader.variableBool( addedBool.formula ); }
    for (auto & addedInt : addedInts)
    { *addedInt.variable = reader.variableInt( addedInt.formula ); }
    // write the tree
    newTree->Fill();
    if ( maxEntries > 0 && newTree->GetEntries() >= maxEntries )
    {
      std::cout << getTime()
                << "  -> Reached maximum number of required entries" << std::endl;
      break;
    }
  }
  if ( maxEntries < 0 && newTree->GetEntries() != elist->GetN() )
  { std::cerr << getTime()
              << "ERROR : Filled " << newTree->GetEntries() << " of " << elist->GetN()
              << " entries" << std::endl; return 1; }

  std::cout << getTime() 
            << "Writing to output file '" << outFileName << "'" << std::endl;
  newTree->Write();
 
  // return
  return 0;
}
