#!/bin/bash

# This would be good to have in snakemake. Do I want to automate training and choosing an optimal BDT? For now, I think I'll leave that step to do by hand, so snakemake runs will have to pause after the combination, then I'll manually do the MVA training, and then tell snakemake to do the last bit. That means I'll need to change this to just take a single file as input

# This applies the output of the current TMVA classifer to all 32 data files from run 1. It does not do any training of its own 

#### MAKE SURE YOU ARE APPLYING THE CORRECT YEARS CLASSIFIER TO THE CORRECT YEARS DATA ####

# The standard ones - changed infilename and outfilename, make sure you get rid of bg-test
#declare -a arr=(
#"RealMagUp_BDhhh_2018_BDPiPiPi_DPiPiKSLL.tuples.root"
#"RealMagUp_BDhhh_2018_BDPiPiPi_DPiPiKSDD.tuples.root"
#"RealMagUp_BDhhh_2018_BDKPiPi_DPiPiKSLL.tuples.root"
#"RealMagUp_BDhhh_2018_BDKPiPi_DPiPiKSDD.tuples.root"
#"RealMagUp_BDhhh_2018_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"RealMagUp_BDhhh_2018_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"RealMagUp_BDhhh_2018_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"RealMagUp_BDhhh_2018_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"RealMagDown_BDhhh_2018_BDPiPiPi_DPiPiKSDD.tuples.root"
#"RealMagDown_BDhhh_2018_BDKPiPi_DPiPiKSLL.tuples.root"
#"RealMagDown_BDhhh_2018_BDPiPiPi_DPiPiKSLL.tuples.root"
#"RealMagDown_BDhhh_2018_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"RealMagDown_BDhhh_2018_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"RealMagDown_BDhhh_2018_BDKPiPi_DPiPiKSDD.tuples.root"
#"RealMagDown_BDhhh_2018_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"RealMagDown_BDhhh_2018_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"MCMagUp_BDhhh_2018_12267150_BDPiPiPi_DPiPiKSLL.tuples.root"
#"MCMagUp_BDhhh_2018_12267150_BDPiPiPi_DPiPiKSDD.tuples.root"
#"MCMagUp_BDhhh_2018_12267150_BDKPiPi_DPiPiKSLL.tuples.root"
#"MCMagUp_BDhhh_2018_12267150_BDKPiPi_DPiPiKSDD.tuples.root"
#"MCMagDown_BDhhh_2018_12267150_BDPiPiPi_DPiPiKSDD.tuples.root"
#"MCMagDown_BDhhh_2018_12267150_BDPiPiPi_DPiPiKSLL.tuples.root"
#"MCMagDown_BDhhh_2018_12267150_BDKPiPi_DPiPiKSLL.tuples.root"
#"MCMagDown_BDhhh_2018_12267150_BDKPiPi_DPiPiKSDD.tuples.root"
#"MCMagUp_BDhhh_2018_12267100_BDKPiPi_DPiPiKSLL.tuples.root"
#"MCMagUp_BDhhh_2018_12267100_BDKPiPi_DPiPiKSDD.tuples.root"
#"MCMagUp_BDhhh_2018_12267100_BDPiPiPi_DPiPiKSDD.tuples.root"
#"MCMagUp_BDhhh_2018_12267100_BDPiPiPi_DPiPiKSLL.tuples.root"
#"MCMagDown_BDhhh_2018_12267100_BDKPiPi_DPiPiKSLL.tuples.root"
#"MCMagDown_BDhhh_2018_12267100_BDKPiPi_DPiPiKSDD.tuples.root"
#"MCMagDown_BDhhh_2018_12267100_BDPiPiPi_DPiPiKSDD.tuples.root"
#"MCMagDown_BDhhh_2018_12267100_BDPiPiPi_DPiPiKSLL.tuples.root"
#)

# Backgrounds test: in rootfiles/bg-test/blah
#declare -a arr=(
#"BGMagUp_BDhhh_2018_11168110_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_11168110_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_11168110_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_11168110_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11168110_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11168110_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_11168110_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11168110_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268100_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_11268100_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_11268100_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_11268100_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_11268100_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268100_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268100_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268100_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268110_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_11268110_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_11268110_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268110_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268110_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_11268110_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268110_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_11268110_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_13168110_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_13168110_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_13168110_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_13168110_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_13168110_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_13168110_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_13168110_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_13168110_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12167100_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12167100_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12167100_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12167100_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12167100_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12167100_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12167100_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12167100_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12197150_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12197150_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12197150_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12197150_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12197150_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12197150_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12197150_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12197150_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267300_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12267300_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267300_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12267300_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267300_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267300_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267300_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267300_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267310_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12267310_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267310_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12267310_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267310_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267310_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267310_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267310_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267500_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12267500_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267500_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12267500_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267500_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267500_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267500_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267500_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267510_BDPiPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12267510_BDPiPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267510_BDKPiPi_DPiPiKSDD.tuples.root"
#"BGMagUp_BDhhh_2018_12267510_BDKPiPi_DPiPiKSLL.tuples.root"
#"BGMagUp_BDhhh_2018_12267510_BDKPiPi_DPiPiKSDD_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267510_BDKPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267510_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
#"BGMagUp_BDhhh_2018_12267510_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
#)

declare -a arr=(
"BGMagUp_BDhhh_2018_11268110_BDKPiPi_DPiPiKSDD.tuples.root"
"BGMagUp_BDhhh_2018_11268110_BDPiPiPi_DPiPiKSDD.tuples.root"
"BGMagUp_BDhhh_2018_11268110_BDKPiPi_DPiPiKSDD_WS.tuples.root"
"BGMagUp_BDhhh_2018_11268110_BDKPiPi_DPiPiKSLL_WS.tuples.root"
"BGMagUp_BDhhh_2018_11268110_BDKPiPi_DPiPiKSLL.tuples.root"
"BGMagUp_BDhhh_2018_11268110_BDPiPiPi_DPiPiKSLL_WS.tuples.root"
"BGMagUp_BDhhh_2018_11268110_BDPiPiPi_DPiPiKSDD_WS.tuples.root"
"BGMagUp_BDhhh_2018_11268110_BDPiPiPi_DPiPiKSLL.tuples.root"
)

for name in "${arr[@]}"
do
    echo "================================================================================================================"
    echo "Running $name"
    tree="BD"
    if echo "$name" | grep -q "KPiPi_"; then
        tree+="KPiPi_DPiPiKS"
    else
        tree+="PiPiPi_DPiPiKS"
    fi
    if echo "$name" | grep -q "LL"; then
        mode="LL"
        tree+="LL"
    else
        mode="DD"
        tree+="DD"
    fi
    if echo "$name" | grep -q "WS"; then
        tree+="_WS"
    fi
    tree+="_Tuple/BDKPiPiTuple"

    infilename="/r01/lhcb/lovell/b2dkpipi/rootfiles/bg-test/combined/$name"
    outdir="/r01/lhcb/lovell/b2dkpipi/rootfiles/bg-test/bdt-applied"
    mkdir -p $outdir # It breaks spectacularly if this doesn't exist, so I'm making sure it does (if it already exists, this does nothing)
    outfilename="$outdir/$name" 

    cp TMVAInputs$mode.txt DataSet/output-$mode/weights/

    # Where the doing happens - I'm pretty certain my string manipulation hasn't broken anything
    TMVAReader.exe --TreeName $tree --Inputs TMVAInputs$mode.txt --InRootFiles $infilename:$tree --OutRootFile $outfilename --WorkDir DataSet/output-$mode/weights --PreSelCutsFile cuts/signalcutsnew.txt 2>&1 | tee logs/TMVARead$name.log

done 

# This could do the hadd step for you, but as it takes no time, and is just another place I'd need to edit the script each time, I won't bother
# The commands would be:
#hadd -ff /r01/lhcb/lovell/b2dkpipi/rootfiles/bdt-applied/all-real-signal-LL.root /r01/lhcb/lovell/b2dkpipi/bdt-applied/Real*KPiPi*LL*
#hadd -ff /r01/lhcb/lovell/b2dkpipi/rootfiles/bdt-applied/all-real-signal-DD.root /r01/lhcb/lovell/b2dkpipi/bdt-applied/Real*KPiPi*DD*
echo "All done!"
exit 0
