#!/bin/bash

# Add the NB Output variable for the above training to various input ROOT files

# Data files
#export ROOTDIR=/castor/cern.ch/user/j/jonrob/NeuroBayesSelTeacher/example
export ROOTDIR=/afs/cern.ch/work/j/jonrob/MVATraining/example
export SIGNALFILE=${ROOTDIR}/signal.root
export BCKGRDFILE=${ROOTDIR}/background.root

rm -f signalWithNBoutput.root
NBExpert.exe --TreeName "B2MuMuTuple/DecayTree" --Inputs NBInputs.txt --InRootFiles $SIGNALFILE --OutRootFile signalWithNBoutput.root --InputExpertiseFile expert.nb

rm -f backgroundWithNBoutput.root
NBExpert.exe --TreeName "B2MuMuTuple/DecayTree" --Inputs NBInputs.txt --InRootFiles $BCKGRDFILE --OutRootFile backgroundWithNBoutput.root --InputExpertiseFile expert.nb

exit 0
