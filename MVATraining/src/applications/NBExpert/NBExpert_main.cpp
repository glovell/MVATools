// STL
#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <utility>

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCut.h"
#include "TEntryList.h"

// local
#include "MVATraining/ParseOptions.h"
#include "MVATraining/ChainNTuples.h"
#include "MVATraining/TCutFromTextFile.h"
#include "MVATraining/NTupleReader.h"
#include "MVATraining/DecodeInputs.h"
#include "MVATraining/GetTime.h"

// NeuroBayes
#include "NeuroBayesExpert.hh"

// Boost
#include <boost/regex.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"

int main( int argc, char** argv )
{

  // Options Parser
  ParseOptions options;
  // Define the options for this application
  options.defineOption( "InRootFiles", "in.files",
                        "Input ROOT File list.\n"
                        "Can either be a path to a ROOT file, a comma separated list\n"
                        "of ROOT files, or a text file that contains a list of ROOT\n"
                        "files, with one file per line." );
  options.defineOption( "OutRootFile",         "out.root",   "Output ROOT File name" );
  options.defineOption( "Inputs",              "inputs.txt", "List of input variables" );
  options.defineOption( "InputExpertiseFile",  "expert.nb",
                        "The trained network expertise file." );
  options.defineOption( "PreSelCutsFile",      "",
                        "PreSelection cuts. An empty string means no cuts." );
  options.defineOption( "TreeName",    "UNDEFINED",
                        "TTree name (including any directories) in the input ROOT file(s)." );
  options.defineOption( "ANNOutputBranchName", "NBANNOut",
                        "The name to give the new Branch in the output TTree that\n"
                        "contains the ANN output variable." );
  // Parse the arguments
  if ( !options.parse(argc,argv) ) { return 1; }

  // Get the list of input files
  const std::string inFiles = options.parameter<std::string>("InRootFiles");

  // Tuple name
  const std::string treeName = options.parameter<std::string>("TreeName");

  // input variables
  typedef DecodeInputs<int> Inputs;
  Inputs inputs;
  if ( !inputs.decode(options.parameter<std::string>("Inputs")) ) return 1;

  // Chain the ROOT files
  auto chain = chainRootFiles( inFiles, treeName );
  if ( !chain ) { return 1; }
  if ( chain->GetEntries() == 0 )
  { std::cerr << getTime() << "ERROR : Empty input chain" << std::endl; return 1; }

  // Create the TCut for the selection
  TCut sel;
  const std::string preSelFileName = options.parameter<std::string>("PreSelCutsFile");
  if ( !preSelFileName.empty() && !createTCut( preSelFileName, sel ) )
  { std::cerr << getTime() << "ERROR : Problem parsing selection cuts" << std::endl; return 1; }

  // Select entries using the presel TCut
  std::cout << getTime() << "Scanning the input TChain to pre-select entries ..." << std::endl;
  chain->Draw( ">> elist", sel, "entrylist" );
  TEntryList * elist = (TEntryList*) gDirectory->Get("elist");
  if ( !elist )
  { std::cerr << getTime() << "ERROR : Problem getting selected entries" << std::endl; return 1; }
  std::cout << getTime() << "  -> Selected " << elist->GetN() << " entries from "
            << chain->GetEntries()
            << " ( " << 100.0 * elist->GetN() / chain->GetEntries() << " % )"
            << std::endl;

  // Open output ROOT file
  const std::string outFileName = options.parameter<std::string>("OutRootFile");
  TFile * outFile = new TFile(outFileName.c_str(),"recreate");
  outFile->cd();
  // Do we have directories in the ROOT file to replicate ?
  const std::string::size_type slash = treeName.find_last_of( "/" );
  if ( slash != std::string::npos )
  {
    const std::string topDir = treeName.substr(0,slash);
    std::cout << getTime() << "Creating ROOT file directory " << topDir << std::endl;
    TDirectory * dir = outFile->mkdir( topDir.c_str() );
    if ( !dir )
    { std::cerr << getTime() << "ERROR : Failed to create ROOT directory " << topDir
                << std::endl; return 1; }
    dir->cd();
  }

  // Clone the TTree structure
  TChain * newchain = (TChain*)chain->CloneTree(0);
  TTree  * newTree  = ( newchain ? newchain->GetTree() : NULL );
  if ( !newTree )
  { std::cerr << getTime() << "ERROR : Failed to clone the TChain" << std::endl; return 1; }

  // Make an NTuple reader
  NTupleReader reader( chain, inputs.formulas() );
  if ( !reader.isOK() ) { return false; }

  // Array for variables to pass to NB
  float InputArray[inputs.inputs().size()];

  // Create the NB Expert
  const std::string nbExpertiseFile = options.parameter<std::string>("InputExpertiseFile");
  std::cout << getTime() << "Using NB expertise file " << nbExpertiseFile << std::endl;
  Expert * expert = new Expert( nbExpertiseFile.c_str() );

  // Add the NB Output to the TTree written out
  double netOut(0);
  const std::string nnOutBName = options.parameter<std::string>("ANNOutputBranchName");
  std::cout << getTime() << "Adding the NB output to branch '" << nnOutBName << "'" 
            << std::endl;
  newTree->Branch( nnOutBName.c_str(), &netOut );

  // Loop over the entries and write out those passing the NB selection
  std::cout << getTime() << "Filling the filtered TTree ..." << std::endl;
  chain->SetEntryList(elist);
  const int nEntries = elist->GetN();
  const int inc = ( nEntries >= 20 ? nEntries/20 : 1 );
  for ( int entry = 0; entry < nEntries; ++entry )
  {
    const int entryN = chain->GetEntryNumber(entry);
    if ( entryN < 0 ) break;
    chain->GetEntry(entryN);
    if ( (entry+1) % inc == 0 )
    {
      std::cout << getTime() << " -> Read entry " << (1+entry)
                << " ( " << 100.0*((float)(1+entry)/(float)nEntries) << " % )"
                << std::endl;
    }

    // Fill input array for the expert
    for ( std::pair<Inputs::Formulas::const_iterator,int> i(inputs.formulas().begin(),0);
          i.first != inputs.formulas().end(); ++i.first, ++i.second )
    {
      InputArray[i.second] = (float)reader.variable(*i.first);
    }

    // NeuroBayes seems to sporadically send mysterious std messages which we
    // cannot control... So forcibly intercept them all here and send to /dev/null
    const int original_stdout = dup(fileno(stdout));
    fflush(stdout);
    freopen("/dev/null","w",stdout);
    const int original_stderr = dup(fileno(stderr));
    fflush(stderr);
    freopen("/dev/null","w",stderr);

    // network output
    netOut = expert->nb_expert(InputArray);

    // put std back to normal
    fflush(stdout);
    dup2(original_stdout,fileno(stdout));
    close(original_stdout);
    fflush(stderr);
    dup2(original_stderr,fileno(stderr));
    close(original_stderr);

    // Write to output tree
    newTree->Fill();
  }
  std::cout << getTime() << "Selected " << newTree->GetEntries() << " from "
            << nEntries << " entries ( "
            << 100.0 * newTree->GetEntries() / nEntries << " % )"
            << std::endl;

  // clean up NB
  delete expert;
  expert = NULL;

  std::cout << getTime()
            << "Writing to output file '" << outFileName << "'" << std::endl;
  newTree->Write();
  outFile->Close();

  return 0;
}
