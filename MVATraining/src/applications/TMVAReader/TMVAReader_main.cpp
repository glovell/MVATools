// STL
#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <utility>
#include <memory>
#include <set>

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCut.h"
#include "TEntryList.h"
#include "Compression.h"
#include "TSystem.h"
#include "TROOT.h"

// TMVA
#include "TMVA/Reader.h"

// local
#include "MVATraining/ParseOptions.h"
#include "MVATraining/ChainNTuples.h"
#include "MVATraining/TCutFromTextFile.h"
#include "MVATraining/NTupleReader.h"
#include "MVATraining/DecodeInputs.h"
#include "MVATraining/GetTime.h"

// Boost
#include <boost/regex.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"
#include <boost/filesystem.hpp>
#include "boost/assign/list_of.hpp"
#include "boost/format.hpp"

typedef DecodeInputs<char> Inputs;

class TMVAReaderInstance final
{
public:
  bool configure( const std::string & dir,
                  const std::string & inputsS,
                  const std::string & spectatorsFile,
                  TChain * chain )
  {
    // Decode the inputs
    bool OK = inputs.decode( dir+"/"+inputsS );
    if ( OK )
    {
      // Make the input array
      InputArray = std::vector<float>( inputs.inputs().size(), 0 );

      // make the reader
      tmvaReader = std::shared_ptr<TMVA::Reader>( new TMVA::Reader("!Color:!Silent:V") );

      // Configure the reader inputs
      for ( std::pair<Inputs::Inputs::const_iterator,int> i(inputs.inputs().begin(),0);
            i.first != inputs.inputs().end(); ++i.first, ++i.second )
      {
        tmvaReader->AddVariable( i.first->c_str(), &(InputArray[i.second] ) );
      }

      // Add spectator variables
      if ( "NONE" != spectatorsFile )
      {
        std::cout << getTime() << "Reading spectator variables" << std::endl;
        OK = spectators.decode( dir+"/"+spectatorsFile );
        if ( OK )
        {
          SpectatorArray = std::vector<float>(spectators.inputs().size(),0);
          for ( std::pair<Inputs::Inputs::const_iterator,int> i(spectators.inputs().begin(),0);
                i.first != spectators.inputs().end(); ++i.first, ++i.second )
          {
            if ( std::find( inputs.inputs().begin(),
                            inputs.inputs().end(),
                            *(i.first) ) == inputs.inputs().end() )
            {
              tmvaReader->AddSpectator( i.first->c_str(), &(SpectatorArray[i.second]) );
            }
            else
            {
              std::cout << getTime()
                        << "Spectator variable '" << *(i.first) << "' is an input -> skipping"
                        << std::endl;
            }
          }
        }
      }
      else
      {
        std::cout << getTime() << "No spectator variables" << std::endl;
      }

      if ( OK )
      {
        // Make an NTuple reader for the variables
        auto tmpFormulas = inputs.formulas();
        if ( "NONE" != spectatorsFile )
        {
          tmpFormulas.insert( tmpFormulas.end(),
                              spectators.formulas().begin(),
                              spectators.formulas().end() );
        }
        tupleReader = std::make_unique<NTupleReader>( chain, tmpFormulas );
        OK = tupleReader->isOK();
      }

    }
    return OK;
  }
  void configOutputs( TTree * newTree )
  {
    // Add the MVA Output(s) to the TTree written out
    for ( auto& mva : mvas ) { newTree->Branch( mva.first.c_str(), &(mva.second) ); }
  }
  void computeOutput()
  {
    // Fill input array for the expert
    for ( std::pair<Inputs::Formulas::const_iterator,int> i(inputs.formulas().begin(),0);
          i.first != inputs.formulas().end(); ++i.first, ++i.second )
    {
      InputArray[i.second] = (float)tupleReader->variable(*i.first);
    }
    // .. and the spectators ..
    if ( !spectators.formulas().empty() )
    {
      for ( std::pair<Inputs::Formulas::const_iterator,int> i(spectators.formulas().begin(),0);
            i.first != spectators.formulas().end(); ++i.first, ++i.second )
      {
        SpectatorArray[i.second] = (float)tupleReader->variable(*i.first);
      }
    }
    // network outputs
    for ( auto& mva : mvas ) { mva.second = tmvaReader->EvaluateMVA(mva.first.c_str()); }
  }
public:
  typedef std::vector<TMVAReaderInstance> Vector;
  typedef std::vector< std::pair<std::string,double> > MVAs;
public:
  std::shared_ptr<TMVA::Reader> tmvaReader;  ///< TMVA Reader
  std::shared_ptr<NTupleReader> tupleReader; ///< NTuple Reader
  MVAs mvas; ///< Vector of MVAs name and output
  Inputs inputs;                     ///< Inputs
  Inputs spectators;                 ///< Spectators
  std::vector<float> InputArray;     ///< Array for inputs
  std::vector<float> SpectatorArray; ///< Array for spectators
};

int main( int argc, char** argv )
{

  // Enable ROOT multi threading
  ROOT::EnableImplicitMT();

  // Options Parser
  ParseOptions options;
  // Define the options for this application
  options.defineOption( "InRootFiles", "in.files",
                        "Input ROOT File list.\n"
                        "Can either be a path to a ROOT file, a comma separated list\n"
                        "of ROOT files, or a text file that contains a list of ROOT\n"
                        "files, with one file per line." );
  options.defineOption( "OutRootFile",  "out.root",   "Output ROOT File name" );
  options.defineOption( "Inputs",       "inputs.txt", "List of input variables" );
  options.defineOption( "Spectators",   "NONE",
                        "List of spectator variables." );
  options.defineOption( "WorkDir",  "weights",
                        "The XML weights directory" );
  options.defineOption( "PreSelCutsFile",      "",
                        "PreSelection cuts. An empty string means no cuts." );
  options.defineOption( "TreeName",    "UNDEFINED",
                        "TTree name (including any directories) in the input ROOT file(s)." );
  // Parse the arguments
  if ( !options.parse(argc,argv) ) { return 1; }

  // Get the list of input files
  const auto inFiles = options.parameter<std::string>("InRootFiles");

  // Tuple name
  const auto treeName = options.parameter<std::string>("TreeName");

  // working Directory
  const auto workDir = options.parameter<std::string>("WorkDir");

  // Chain the ROOT files
  const auto chain = chainRootFiles(inFiles,treeName);
  if ( !chain.get() ) { return 1; }
  if ( chain->GetEntries() == 0 )
  { std::cerr << getTime() << "ERROR : Empty input chain" << std::endl; return 1; }

  // Search the weights dir
  // Decompose a comma seperated list into a vector of strings
  std::vector<std::string> worksDirs;
  boost::split( worksDirs, workDir, boost::is_any_of(",") );
  std::sort( worksDirs.begin(), worksDirs.end() );

  // Make a vector of Reader instances
  TMVAReaderInstance::Vector mvaInstances;
  mvaInstances.reserve( worksDirs.size() );

  // Gloval list of MVA names, to check for overlaps
  std::set<std::string> gMVANames;

  // loop over weights dirs
  for ( auto& dir : worksDirs )
  {
    std::cout << getTime()
              << "Processing work directory '" << dir << "'"
              << std::endl;

    // Make a reader instance
    mvaInstances.emplace_back();

    // configure the inputs
    if ( !mvaInstances.back().configure( dir,
                                         options.parameter<std::string>("Inputs"),
                                         options.parameter<std::string>("Spectators"),
                                         &*chain ) )
    {
      return 1;
    }

    std::cout << getTime() << "Searching for TMVA weight files" << std::endl;
    const boost::regex weightsFilter( "(.*).weights.xml" );
    boost::filesystem::recursive_directory_iterator end_itr;
    for ( boost::filesystem::recursive_directory_iterator i(dir);
          i != end_itr; ++i )
    {
      // Skip if not a file
      if ( !boost::filesystem::is_regular_file(i->status()) ) continue;

      // Skip if no match
      boost::cmatch matches;
      if ( !boost::regex_match( i->path().filename().string().c_str(),
                                matches, weightsFilter ) ) continue;

      // File matches
      const std::string file = ( i->path().parent_path().string() + "/" +
                                 i->path().filename().string() );

      // name
      auto name = i->path().filename().string();
      boost::replace_all(name,".weights.xml","");
      std::cout << getTime()
                << " -> Adding MVA '" << name << "'"
                << " Weights='" << file << "'"
                << std::endl;

      // save list of MVA names
      mvaInstances.back().mvas.emplace_back( name, 0 );
      if ( !gMVANames.insert(name).second )
      {
        std::cerr << getTime()
                  << "MVA name '" << name << "' already exists" << std::endl;
        return 1;
      }

      // add to reader
      mvaInstances.back().tmvaReader->BookMVA( name.c_str(), file.c_str() );
    }

    // Sort the list of MVA names
    std::sort( mvaInstances.back().mvas.begin(),
               mvaInstances.back().mvas.end() );

    // Did we find any MVAs to add
    if ( mvaInstances.back().mvas.empty() )
    {
      std::cout << "No MVAs found for " << dir << std::endl;
      mvaInstances.pop_back();
    }

  }
  if ( mvaInstances.empty() )
  {
    std::cerr << getTime() << "No MVA weight files found -> Abort." << std::endl;
    return 1;
  }

  // Create the TCut for the selection
  TCut sel;
  const auto preSelFileName = options.parameter<std::string>("PreSelCutsFile");
  if ( !preSelFileName.empty() && !createTCut( preSelFileName, sel ) )
  { std::cerr << getTime() << "ERROR : Problem parsing selection cuts" << std::endl;
    return 1; }

  // Select entries using the presel TCut
  std::cout << getTime()
            << "Scanning the input TChain to pre-select entries ..."
            << std::endl;
  chain->Draw( ">> elist", sel, "entrylist" );
  auto elist = (TEntryList*) gDirectory->Get("elist");
  if ( !elist )
  { std::cerr << getTime() << "ERROR : Problem getting selected entries" << std::endl;
    return 1; }
  std::cout << getTime() << " -> Selected " << elist->GetN() << " entries from "
            << chain->GetEntries()
            << " ( " << 100.0 * elist->GetN() / chain->GetEntries() << " % )"
            << std::endl;

  // Open output ROOT file
  const auto outFileName = options.parameter<std::string>("OutRootFile");
  std::unique_ptr<TFile> outFile ( TFile::Open(outFileName.c_str(),"recreate") );
  outFile->SetCompressionSettings( chain->GetFile() ?
                                   chain->GetFile()->GetCompressionSettings() :
                                   ROOT::CompressionSettings(ROOT::kLZMA,4) );
  outFile->cd();
  // Do we have directories in the ROOT file to replicate ?
  const auto slash = treeName.find_last_of( "/" );
  if ( slash != std::string::npos )
  {
    const auto topDir = treeName.substr(0,slash);
    std::cout << getTime() << "Creating ROOT file directory " << topDir << std::endl;
    auto dir = outFile->mkdir( topDir.c_str() );
    if ( !dir )
    { std::cerr << getTime() << "ERROR : Failed to create ROOT directory " << topDir
                << std::endl; return 1; }
    dir->cd();
  }

  // Clone the TTree structure
  auto newchain = (TChain*)chain->CloneTree(0);
  auto newTree  = ( newchain ? newchain->GetTree() : nullptr );
  if ( !newTree )
  { std::cerr << getTime() << "ERROR : Failed to clone the TChain" << std::endl;
    return 1; }

  // Configure the MVAs to be written out to the new TTree
  for ( auto& mva : mvaInstances ) { mva.configOutputs( newTree ); }

  // Loop over the entries and write out those passing the MVA selection
  std::cout << getTime() << "Filling the filtered TTree ..." << std::endl;
  chain->SetEntryList(elist);
  const auto nEntries = elist->GetN();
  const auto inc = ( nEntries >= 20 ? nEntries/20 : 1 );
  for ( int entry = 0; entry < nEntries; ++entry )
  {
    const auto entryN = chain->GetEntryNumber(entry);
    if ( entryN < 0 ) break;
    chain->GetEntry(entryN);
    if ( (entry+1) % inc == 0 )
    {
      const auto pc = (100.0*((float)(1+entry)/(float)nEntries));
      std::cout << getTime()
                << " -> Read entry " << boost::format("%12i") % (1+entry)
                << " ( " << boost::format("%3.0f") % pc << " % )"
                << std::endl;
    }

    // Compute the inputs and outputs for each MVA
    for ( auto& mva : mvaInstances ) { mva.computeOutput(); }

    // Write to output tree
    newTree->Fill();
  }

  std::cout << getTime() << "Selected " << newTree->GetEntries() << " from "
            << nEntries << " entries ( "
            << 100.0 * newTree->GetEntries() / nEntries << " % )"
            << std::endl;

  std::cout << getTime()
            << "Writing to output file '" << outFileName << "'" << std::endl;
  newTree->Write();
  std::cout << getTime() << "Done..." << std::endl;

  return 0;
}
